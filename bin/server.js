#!/usr/bin/env node
'use strict';

require('../src/siamclassic').createSiamClassicApp()
    .then(function(app) {
        const PORT = process.env.PORT;
        const server = app.listen(PORT, function() {
            app.logger.info('Listening on port:', server.address().port);
        });
    })
    .catch(function(err) {
        console.error('Error creating app');
        console.error(new Date(), err);
        
        process.exit(1);
    });