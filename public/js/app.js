let app = angular.module('SiamClassic', ["ngRoute", "ui.bootstrap", "ngTouch"]);

app.config(function ($routeProvider, $httpProvider) {

    $routeProvider.when("/Home", {templateUrl: "../views/home.html", controller: "homeController"})
        .when("/Home/menu", {templateUrl: "../views/menu.html", controller: "menuController"})
        .when("/Home/reviews", {templateUrl: "../views/reviews.html", controller: "reviewController"})
        .when("/Home/about", {templateUrl: "../views/about.html", controller: "aboutController"})
        .otherwise({redirectTo: "/Home"});
});

app.controller("indexController", ["$rootScope", "$scope", "$location", "$window", "$timeout", "modalControllers", "SessionService",
    function ($rootScope, $scope, $location, $window, $timeout, modalControllers, SessionService) {
        /*Boolean that controls navbar options*/
        $scope.isOrdering = function () {
            return SessionService.isOrdering()
        };

        /*Boolean that controls affix buttons*/
        $scope.hasCustomerInfo = function () {
            return SessionService.hasCustomerInfo()
        };

        /*Initiates a new online order*/
        $scope.orderOnlineStart = function () {
            /*if (SessionService.isOrdering()) {
                modalControllers.shopCart();
            }
            else {
                if ($scope.hasCustomerInfo()) {
                    modalControllers.orderSetUp();
                }
                else {
                    modalControllers.orderInit();
                }
            }*/
            $window.open('http://eat24hrs.com/restaurants/order2/index.php?id=100618');
        };

        /*Opens shopping cart from any page*/
        $scope.openCart = function () {
            modalControllers.shopCart()
        };

        /*Opens login modal*/
        $scope.openLogin = function () {
            modalControllers.loginModal();
        };

        /*Opens signup modal*/
        $scope.openSignUp = function () {
            modalControllers.signupModal();
        };

        /*Opens user modal to view/change account info (guests can not change info)*/
        $scope.openUser = function () {
            modalControllers.userModal();
        };

        /*Switches current page*/
        $scope.goTo = function (link) {
            $location.path(link);
        };

        /*Opens link in seperate tab*/
        $scope.goToExternal = function (link) {
            $window.open(link);
        };

        /*Controls which tab will be open when going to the menu*/
        $scope.setTabIndex = function (num) {
            SessionService.setTabIndex(num);
            $location.path('/Home/menu');
        };

        /*Ends current online order reseting session variables*/
        $scope.orderOnlineEnd = function () {
            let order = [];
            let cost = 0.0;
            let tax = 0.0;
            let totalCost = 0.0;
            let deliFee = 0;
            SessionService.saveOrder(order, cost, tax, totalCost, deliFee);
            SessionService.clear();
            cancelOrder()
        };
        $scope.userLogout = function () {
            if (SessionService.isOrdering()) {
                let order = [];
                let cost = 0.0;
                let tax = 0.0;
                let totalCost = 0.0;
                let deliFee = 0;
                SessionService.saveOrder(order, cost, tax, totalCost, deliFee);
                SessionService.clear();
                SessionService.logout();
            }
            else {
                SessionService.logout();
            }
            cancelOrder()
        };

        /*Closes navbar after selection*/
        $('#mainNav a').click(function () {
            $(".navbar-collapse").collapse('hide');
        });
        $(".nav a").on("click", function () {
            $(".nav").find(".active").removeClass("active");
            $(this).parent().addClass("active");
        });

        $scope.$on('wait', function (event, args) {
            $scope.wait = args;
        });
        $scope.$on('cancelOrder', function () {
            cancelOrder()
        });
        $scope.$on('updateInfo', function () {
            updateInfo()
        });

        let cancelOrder = function () {
            $scope.showCancelAlert = true;
            $timeout(function () {
                $scope.showCancelAlert = false;
            }, 2000)
        };
        let updateInfo = function () {
            $scope.showUpdateAlert = true;
            $timeout(function () {
                $scope.showUpdateAlert = false;
            }, 3000)
        };

        $scope.showCancelAlert = false;
        $scope.showUpdateAlert = false;

        $(window).on('beforeunload', function () {
            $(window).scrollTop(0);

        });
        $scope.$on('$locationChangeSuccess', function () {
            $scope.currentPage = $location.path();
        });

    }]);

app.controller("menuController", ["$scope", "$window", "SessionService", "modalControllers", "retrieve", "$timeout",
    function ($scope, $window, SessionService, modalControllers, retrieve, $timeout) {
        $(function () {
            $scope.menuCat = ["SPECIALS", "APPETIZERS", "SOUPS & SALADS", "NOODLES & FRIED RICE", "ENTRE\xc9S", "HOUSE SUGGESTIONS", "VEGETARIAN", "CURRY", "DESSERTS", "DRINKS"];

            $('#sidebar').affix({
                offset: {
                    top: 0,
                    bottom: 400
                }
            });
            updateOrder();
            if (SessionService.hasTabIndex()) {
                $scope.selectedView = SessionService.getTabIndex();
                $scope.selectedCat = $scope.menuCat[$scope.selectedView];
            }
            else {
                $scope.selectedView = 0;
                $scope.selectedCat = $scope.menuCat[$scope.selectedView];
            }
        });

        /*GET menu items and modifiers information)*/
        retrieve.getMenuItems()
            .then(function (data) {
                $scope.fullMenu = [
                    [{name: 'APPETIZER SPECIALS', items: data.menu_items.menuSpecA}, {name: 'ENTRE\xc9 SPECIALS', items: data.menu_items.menuSpecE}],
                    [{name: 'APPETIZERS', items: data.menu_items.menuApp}],
                    [{name: 'SOUPS & SALADS', items: data.menu_items.menuSS}],
                    [{name: 'NOODLES & FRIED RICE', items: data.menu_items.menuNFR}],
                    [{name: 'ENTRE\xc9S', items: data.menu_items.menuEnt}],
                    [{name: 'HOUSE SUGGESTIONS', items: data.menu_items.menuHS}],
                    [{name: 'VEGETARIAN', items: data.menu_items.menuVeg}],
                    [{name: 'CURRY', items: data.menu_items.menuCur}],
                    [{name: 'DESSERTS', items: data.menu_items.menuDes}],
                    [{name: 'DRINKS', items: data.menu_items.menuDri}, {name: 'ESPRESSOS', items: data.menu_items.menuEsp}]
                ];
                retrieve.getMenuItemModifierGroup()
                    .then(function (data) {
                        $scope.menuItemModGroup = data.menuitem_modifiergroup;
                        retrieve.getMenuModifiers()
                            .then(function (data) {
                                $scope.menuModifiers = [];
                                let acceptedModifiers = [10, 12, 17, 22, 96, 97, 98, 100, 101, 102, 103, 104, 105, 106, 107];
                                for (let i = 0; i < data.menu_modifier.length; i++) {
                                    if (jQuery.inArray(data.menu_modifier[i].group_id, acceptedModifiers) !== -1) {
                                        $scope.menuModifiers.push(data.menu_modifier[i]);
                                    }
                                }
                                $scope.menuModifiers.sort(function (a, b) {
                                    let aPrice = a.price;
                                    let bPrice = b.price;
                                    return ((aPrice < bPrice) ? -1 : ((aPrice > bPrice) ? 1 : 0));
                                });
                            });
                    });

            });

        /*Adds selected item to order by one*/
        $scope.addItemToOrder = function (item) {
            let menuItemModGroupArray = [];
            let itemsWithRice = [25, 27, 28, 29];
            for (let i = 0; i < $scope.menuItemModGroup.length; i++) {
                if (item.id === $scope.menuItemModGroup[i].menuitem_modifiergroup_id) {
                    menuItemModGroupArray.push($scope.menuItemModGroup[i].modifier_group);
                }
            }
            let parameters = {
                hasChoice: jQuery.inArray(10, menuItemModGroupArray) !== -1,
                hasSpice: jQuery.inArray(17, menuItemModGroupArray) !== -1,
                hasExtras: jQuery.inArray(96, menuItemModGroupArray) !== -1,
                hasRice: jQuery.inArray(item.group_id, itemsWithRice) !== -1,
                isSalmon: jQuery.inArray(100, menuItemModGroupArray) !== -1,
                isSoda: jQuery.inArray(101, menuItemModGroupArray) !== -1,
                isJuice: jQuery.inArray(102, menuItemModGroupArray) !== -1,
                isSmoothie: jQuery.inArray(103, menuItemModGroupArray) !== -1,
                isTea: jQuery.inArray(104, menuItemModGroupArray) !== -1,
                isCoffee: jQuery.inArray(105, menuItemModGroupArray) !== -1,
                hasBoba: jQuery.inArray(106, menuItemModGroupArray) !== -1,
                isEspresso: jQuery.inArray(107, menuItemModGroupArray) !== -1,
                isIceCream: jQuery.inArray(12, menuItemModGroupArray) !== -1,
                isFish: jQuery.inArray(22, menuItemModGroupArray) !== -1
            };
            modalControllers.addItem(item, parameters, $scope.menuModifiers);
        };

        /*Broadcasts to show relevant alerts and updates orders after 'Add' modal*/
        $scope.$on('addToOrder', function () {
            updateOrder();
            $scope.showAddAlert = true;
            $timeout(function () {
                $scope.showAddAlert = false;
            }, 2000)
        });
        $scope.$on('clearOrder', function () {
            $scope.showClearAlert = true;
            $timeout(function () {
                $scope.showClearAlert = false;
            }, 2000)
        });

        $scope.$on('createOrder', function () {
            $scope.showCreateAlert = true;
            $timeout(function () {
                $scope.showCreateAlert = false;
            }, 3000)
        });

        $scope.$on('updateOrder', function () {
            updateOrder()
        });

        /*Init alerts' visibility*/
        $scope.showAddAlert = false;
        $scope.showClearAlert = false;
        $scope.showCreateAlert = false;

        $scope.selected = function (i) {
            $(window).scrollTop(0);
            $scope.selectedView = i;
            $scope.selectedCat = $scope.menuCat[i];
            SessionService.setTabIndex(i);
        };


        /*------Code for Shopping cart-------*/
        function updateOrder() {
            $scope.hasOrder = SessionService.hasOrder();
            $scope.orderInfo = angular.fromJson(SessionService.getOrderInfo());
            $scope.distance = SessionService.getDeliveryDistance();
            if ($scope.hasOrder) {
                let order = angular.fromJson(SessionService.getOrder());
                $scope.order = order.order;
                $scope.cost = order.cost;
                $scope.tax = order.tax;
                if ($scope.orderInfo.type === 'Delivery') {
                    if ($scope.cost > 20.0) {
                        $scope.deliFee = 0.0;
                    }
                    else {
                        $scope.deliFee = Math.ceil($scope.distance / 1609.34);
                        console.log($scope.deliFee)
                    }
                }
                else {
                    $scope.deliFee = 0.0;
                }
                $scope.totalCost = $scope.cost + $scope.tax + $scope.deliFee;
                SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
            }
        }

        /*+1 of desired item to order*/
        $scope.increaseQty = function (index) {
            $scope.order[index].quantity = parseInt($scope.order[index].quantity) + 1;
            $scope.cost = $scope.cost + parseFloat($scope.order[index].indivPrice);
            if ($scope.orderInfo.type === 'Delivery') {
                if ($scope.cost > 20.0) {
                    $scope.deliFee = 0;
                }
                else {
                    $scope.deliFee = Math.ceil($scope.distance / 1609.34);
                    console.log($scope.deliFee)
                }
            }
            else {
                $scope.deliFee = 0;
            }
            $scope.tax = $scope.cost * (0.1);
            $scope.totalCost = $scope.tax + $scope.cost + $scope.deliFee;
            SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
        };

        /*-1 of desired item to order*/
        $scope.decreaseQty = function (index) {
            if (parseInt($scope.order[index].quantity) > 1) {
                $scope.order[index].quantity = parseInt($scope.order[index].quantity) - 1;
                $scope.cost = $scope.cost - parseFloat($scope.order[index].indivPrice);
                if ($scope.orderInfo.type === 'Delivery') {
                    if ($scope.cost > 20.0) {
                        $scope.deliFee = 0;
                    }
                    else {
                        $scope.deliFee = Math.ceil($scope.distance / 1609.34);
                        console.log($scope.deliFee)
                    }
                }
                else {
                    $scope.deliFee = 0;
                }
                $scope.tax = $scope.cost * (0.1);
                $scope.totalCost = $scope.tax + $scope.cost + $scope.deliFee;
                SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
            }
        };

        /*Removes entire quantity of item from order*/
        $scope.remove = function (index) {
            $scope.cost = $scope.cost - parseFloat($scope.order[index].indivPrice * $scope.order[index].quantity);
            if ($scope.orderInfo.type === 'Delivery') {
                if ($scope.cost > 20.0) {
                    $scope.deliFee = 0;
                }
                else {
                    $scope.deliFee = Math.ceil($scope.distance / 1609.34);
                    console.log($scope.deliFee)
                }
            }
            else {
                $scope.deliFee = 0;
            }
            $scope.tax = $scope.cost * (0.1);
            $scope.totalCost = $scope.tax + $scope.cost + $scope.deliFee;
            $scope.order.splice(index, 1);
            SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
            if ($scope.cost < 0.01) {
                SessionService.removeOrder();
                $scope.hasOrder = false;
            }
        };

        $scope.orderPay = function () {
            modalControllers.orderPay();
        };

        $scope.clear = function () {
            $scope.order = [];
            $scope.cost = 0;
            $scope.tax = 0;
            $scope.totalCost = 0;
            $scope.deliFee = 0;
            SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
            SessionService.removeOrder();
        };

        $scope.cancel = function () {
            $scope.order = [];
            $scope.cost = 0;
            $scope.tax = 0;
            $scope.totalCost = 0;
            $scope.deliFee = 0;
            SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
            SessionService.clear();
            $rootScope.$broadcast('cancelOrder');
        };


    }]);
app.controller("homeController", ["$rootScope", "$scope", "$location", "$window", "modalControllers", "SessionService", "retrieve",
    function ($rootScope, $scope, $location, $window, modalControllers, SessionService, retrieve) {
        retrieve.getMenuItemsSpecials().then(function (data) {
            $scope.menuSpecials  = data.menu_items;
        })

    }]);
app.controller("reviewController", ["$rootScope", "$scope", "$location", "$window", "modalControllers", "SessionService",
    function ($rootScope, $scope, $location, $window, modalControllers, SessionService) {

    }]);
app.controller("aboutController", ["$rootScope", "$scope", "$location", "$window", "modalControllers", "SessionService",
    function ($rootScope, $scope, $location, $window, modalControllers, SessionService) {

    }]);


