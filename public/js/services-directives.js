/**
 * Created by Ethan on 4/18/2017.
 */
app.service("SessionService", function ($injector, $window) {
    // Ordering operations
    this.setOrdering = function (token) {
        $window.sessionStorage.setItem('order-online', token);
    };
    this.removeOrdering = function () {
        $window.sessionStorage.removeItem('order-online');
    };
    this.isOrdering = function () {
        return (!!$window.sessionStorage.getItem('order-online'));
    };

    // Order Operations
    this.saveOrder = function (order, cost, tax, totalCost, deliFee) {
        let orderPreview = {order: order, cost: cost, tax: tax, totalCost: totalCost, deliFee: deliFee};
        $window.sessionStorage.setItem('order', angular.toJson(orderPreview));
    };
    this.removeOrder = function () {
        $window.sessionStorage.removeItem('order');
    };
    this.getOrder = function () {
        return $window.sessionStorage.getItem('order');
    };
    this.hasOrder = function () {
        return (!!$window.sessionStorage.getItem('order'));
    };


    // Delivery operations
    this.setDeliveryDistance = function (dist) {
        $window.sessionStorage.setItem('delivery', dist);
    };
    this.removeDelivery = function () {
        $window.sessionStorage.removeItem('delivery');
    };
    this.getDeliveryDistance = function () {
        return $window.sessionStorage.getItem('delivery');
    };
    this.isDeliveryRange = function () {
        return this.getDeliveryDistance() <= 8046.72;
    };
    this.isDelivery = function () {
        return (!!$window.sessionStorage.getItem('delivery'));
    };

    // Tab operations
    this.setTabIndex = function (index) {
        $window.sessionStorage.setItem('tab-index', index);
    };
    this.getTabIndex = function () {
        return $window.sessionStorage.getItem('tab-index');
    };
    this.hasTabIndex = function () {
        return (!!$window.sessionStorage.getItem('tab-index'));
    };

    // Customer information operations
    this.setCustomerInfo = function (id, email, name, address, phone, zip) {
        let info = {id: id, email: email, name: name, address: address, phone: phone, zip: zip};
        $window.sessionStorage.setItem('customer-info', angular.toJson(info));
    };
    this.getCustomerInfo = function () {
        return $window.sessionStorage.getItem('customer-info');
    };
    this.removeCustomerInfo = function () {
        return $window.sessionStorage.removeItem('customer-info');
    };
    this.hasCustomerInfo = function () {
        return (!!$window.sessionStorage.getItem('customer-info'));
    };

    // Order info operations
    this.setOrderInfo = function (type, date, time) {
        let info = {type: type, date: date, time: time};
        $window.sessionStorage.setItem('order-info', angular.toJson(info));
    };
    this.getOrderInfo = function () {
        return $window.sessionStorage.getItem('order-info');
    };
    this.removeOrderInfo = function () {
        return $window.sessionStorage.removeItem('order-info');
    };
    this.hasOrderInfo = function () {
        return (!!$window.sessionStorage.getItem('order-info'));
    };

    this.clear = function () {
        this.removeOrdering();
        this.removeOrder();
        this.removeOrderInfo();
    };
    this.logout = function () {
        this.removeCustomerInfo();
        this.removeDelivery();
    };
});

app.service("LocationService", function () {
    this.getDistanceFromRestaurant = function (addressString) {
        let geocoder = new google.maps.Geocoder;
        return new Promise(function (resolve, reject) {
            geocoder.geocode({'address': addressString}, function (results, status) {
                if (status === 'OK') {
                    let lat = results[0].geometry.location.lat();
                    let lng = results[0].geometry.location.lng();
                    let siamLat = 38.7517314;
                    let siamLong = -77.47056179999998;
                    let position1Pos = new google.maps.LatLng(lat, lng);
                    let position2Pos = new google.maps.LatLng(siamLat, siamLong);
                    let distance = google.maps.geometry.spherical.computeDistanceBetween(position1Pos, position2Pos);
                    resolve(distance);
                } else {
                    alert('Invalid Address');
                }
            });
        });
    };
});

app.factory('retrieve', function ($http) {
    return {
        getMenuGroups: function () {
            return $http({
                method: 'GET',
                url: '/getMenuGroups',
                headers: {
                    'Content-type': 'application/json'
                }
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        getMenuItems: function () {
            return $http({
                method: 'GET',
                url: '/getMenuItems',
                headers: {
                    'Content-type': 'application/json'
                }
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        getMenuItemModifierGroup: function () {
            return $http({
                method: 'GET',
                url: '/getMenuItemModifierGroup',
                headers: {
                    'Content-type': 'application/json'
                }
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        getMenuItemsSpecials: function () {
            return $http({
                method: 'GET',
                url: '/getMenuItemsSpecials',
                headers: {
                    'Content-type': 'application/json'
                }
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        getMenuModifiers: function () {
            return $http({
                method: 'GET',
                url: '/getMenuModifiers',
                headers: {
                    'Content-type': 'application/json'
                }
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        login: function (customerInfo) {
            return $http({
                method: 'POST',
                url: '/signin',
                headers: {
                    'Content-type': 'application/json'
                },
                data: customerInfo
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        payOrder: function (orderInfo) {
            return $http({
                method: 'POST',
                url: '/authorizePayment',
                headers: {
                    'Content-type': 'application/json'
                },
                data: orderInfo
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        signup: function (customerInfo) {
            return $http({
                method: 'POST',
                url: '/signup',
                headers: {
                    'Content-type': 'application/json'
                },
                data: customerInfo
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        updateCustomer: function (customerInfo) {
            return $http({
                method: 'POST',
                url: '/updateCustomer',
                headers: {
                    'Content-type': 'application/json'
                },
                data: customerInfo
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        updatePassword: function (pwInfo) {
            return $http({
                method: 'POST',
                url: '/updatePassword',
                headers: {
                    'Content-type': 'application/json'
                },
                data: pwInfo
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        },
        /*test: function (temp) {
            return $http({
                method: 'POST',
                url: '/getMenuItemsByGroups',
                headers: {
                    'Content-type': 'application/json'
                },
                data: temp
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                console.log("errorCallback " + response.data);
            });
        }*/
    }
});

app.factory('modalControllers', function ($http, $uibModal) {
    return {
        addItem: function (item, parameters, modifiers) {
            $uibModal.open({
                templateUrl: '../views/modals/addItem_modal.html',    // HTML for the modal
                controller: function ($rootScope, $scope, $uibModalInstance, SessionService) {
                    let order = {order: [], cost: 0, tax: 0, totalCost: 0, deliFee: 0};
                    let orderInfo = angular.fromJson(SessionService.getOrderInfo());
                    let distance = SessionService.getDeliveryDistance();
                    if (SessionService.hasOrder()) {
                        order = angular.fromJson(SessionService.getOrder())
                    }
                    $scope.item = item;
                    $scope.hasSpice = parameters.hasSpice;
                    $scope.hasExtras = parameters.hasExtras;
                    $scope.hasRice = parameters.hasRice;
                    $scope.hasBoba = parameters.hasBoba;
                    $scope.spiceLevel = 0;
                    $scope.spiceLevelText = "Spiciness";
                    $scope.hasModifiers = false;
                    $scope.menuModifiers = [];
                    let choiceModifiers = [];
                    let iceCreamModifiers = [];
                    let fishModifiers = [];
                    let salmonModifiers = [];
                    let sodaModifiers = [];
                    let juiceModifiers = [];
                    let smoothieModifiers = [];
                    let teaModifiers = [];
                    let coffeeModifiers = [];
                    let espressoModifiers = [];
                    $scope.proteinExtras = [];
                    $scope.veggieExtras = [];
                    $scope.sideExtras = [];
                    $scope.numberOfProteinExtras = 0;
                    $scope.numberOfSideExtras = 0;
                    $scope.numberOfVeggieExtras = 0;
                    $scope.riceType = 'White';
                    $scope.ricePrice = 0.0;
                    $scope.bobaPrice = 0;
                    $scope.getNumberOfExtras = function (num) {
                        return new Array(num);
                    };
                    $scope.addExtra = function (type) {
                        if (type === 'Protein') {
                            $scope.numberOfProteinExtras += 1;
                        }
                        else if (type === 'Side') {
                            $scope.numberOfSideExtras += 1;
                        }
                        else if (type === 'Veggie') {
                            $scope.numberOfVeggieExtras += 1;
                        }
                    };
                    $scope.removeExtra = function (type) {
                        if (type === 'Protein') {
                            $scope.numberOfProteinExtras -= 1;
                        }
                        else if (type === 'Side') {
                            $scope.numberOfSideExtras -= 1;
                        }
                        else if (type === 'Veggie') {
                            $scope.numberOfVeggieExtras -= 1;
                        }
                    };
                    for (let i = 0; i < modifiers.length; i++) {
                        switch (modifiers[i].group_id) {
                            case 10:
                                if (modifiers[i].price > 0) {
                                    choiceModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    choiceModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 12:
                                if (modifiers[i].price > 0) {
                                    iceCreamModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    iceCreamModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 22:
                                if (modifiers[i].price > 0) {
                                    fishModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    fishModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 100:
                                if (modifiers[i].price > 0) {
                                    salmonModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    salmonModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 101:
                                if (modifiers[i].price > 0) {
                                    sodaModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    sodaModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 102:
                                if (modifiers[i].price > 0) {
                                    juiceModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    juiceModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 103:
                                if (modifiers[i].price > 0) {
                                    smoothieModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    smoothieModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 104:
                                if (modifiers[i].price > 0) {
                                    teaModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    teaModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 105:
                                if (modifiers[i].price > 0) {
                                    coffeeModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    coffeeModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 107:
                                if (modifiers[i].price > 0) {
                                    espressoModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    espressoModifiers.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 96:
                                if (modifiers[i].price > 0) {
                                    $scope.proteinExtras.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    $scope.proteinExtras.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 97:
                                if (modifiers[i].price > 0) {
                                    $scope.veggieExtras.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    $scope.veggieExtras.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                            case 98:
                                if (modifiers[i].price > 0) {
                                    $scope.sideExtras.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name + ' (+ $' + modifiers[i].price + '.00)'
                                    })
                                }
                                else {
                                    $scope.sideExtras.push({
                                        mod: modifiers[i],
                                        string: '' + modifiers[i].translated_name
                                    })
                                }
                                break;
                        }
                    }
                    if (parameters.hasChoice) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = choiceModifiers;
                    }
                    if (parameters.isIceCream) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = iceCreamModifiers;
                    }
                    if (parameters.isFish) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = fishModifiers;
                    }
                    if (parameters.isSalmon) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = salmonModifiers;
                    }
                    if (parameters.isSoda) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = sodaModifiers;
                    }
                    if (parameters.isJuice) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = juiceModifiers;
                    }
                    if (parameters.isSmoothie) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = smoothieModifiers;
                    }
                    if (parameters.isTea) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = teaModifiers;
                    }
                    if (parameters.isCoffee) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = coffeeModifiers;
                    }
                    if (parameters.isEspresso) {
                        $scope.hasModifiers = true;
                        $scope.menuModifiers = espressoModifiers;
                    }
                    $scope.add = function () {
                        let currentVal = parseInt($('#quantity').val());
                        currentVal += 1;
                        $('#quantity').val(currentVal);
                    };
                    $scope.minus = function () {
                        let currentVal = parseInt($('#quantity').val());
                        if (currentVal !== 1) {
                            currentVal -= 1;
                            $('#quantity').val(currentVal);
                        }
                    };
                    $scope.setSpice = function (num) {
                        $scope.spiceLevel = num;
                        $scope.spiceLevelText = '' + num;
                    };
                    $scope.setRiceType = function (type) {
                        switch (type) {
                            case 'White':
                                $scope.riceType = 'White';
                                $scope.ricePrice = 0;
                                break;
                            case 'Brown':
                                $scope.riceType = 'Brown';
                                $scope.ricePrice = 0;
                                break;
                            case 'Fried':
                                $scope.riceType = 'Fried';
                                $scope.ricePrice = 2;
                                break;
                        }
                    };
                    $scope.setBobaPrice = function (num) {
                        $scope.bobaPrice = num;
                    };
                    $scope.submit = function () {
                        let mod = [];
                        let proteinExtras = [];
                        let sideExtras = [];
                        let veggieExtras = [];
                        let extras = [];
                        let quantity = $('#quantity').val();
                        let specialInstructions = 'no special instructions';
                        let whitespaceCheck = $scope.specIns;
                        let itemPrice = 0.0;
                        for (var i = 0; i < $scope.menuModifiers.length; i++) {
                            if ($('#modifiers option:selected').text() === $scope.menuModifiers[i].string) {
                                mod = {
                                    id: $scope.menuModifiers[i].mod.id,
                                    name: $scope.menuModifiers[i].mod.name,
                                    translated_name: $scope.menuModifiers[i].mod.translated_name,
                                    price: $scope.menuModifiers[i].mod.price,
                                    group_id: $scope.menuModifiers[i].mod.group_id
                                };
                                break;
                            }
                        }
                        for (let i = 0; i < $scope.numberOfProteinExtras; i++) {
                            for (let j = 0; j < $scope.proteinExtras.length; j++) {
                                if ($('#proteinExtras-' + i + ' option:selected').text() === $scope.proteinExtras[j].string) {
                                    proteinExtras.push({
                                        id: $scope.proteinExtras[j].mod.id,
                                        name: $scope.proteinExtras[j].mod.name,
                                        translated_name: $scope.proteinExtras[j].mod.translated_name,
                                        price: $scope.proteinExtras[j].mod.price,
                                        group_id: $scope.proteinExtras[j].mod.group_id
                                    });
                                    break;
                                }
                            }
                            extras.push(proteinExtras[i]);
                        }
                        for (let i = 0; i < $scope.numberOfSideExtras; i++) {
                            for (let j = 0; j < $scope.sideExtras.length; j++) {
                                if ($('#sideExtras-' + i + ' option:selected').text() === $scope.sideExtras[j].string) {
                                    sideExtras.push({
                                        id: $scope.sideExtras[j].mod.id,
                                        name: $scope.sideExtras[j].mod.name,
                                        translated_name: $scope.sideExtras[j].mod.translated_name,
                                        price: $scope.sideExtras[j].mod.price,
                                        group_id: $scope.sideExtras[j].mod.group_id
                                    });
                                    break;
                                }
                            }
                            extras.push(sideExtras[i]);
                        }
                        for (let i = 0; i < $scope.numberOfVeggieExtras; i++) {
                            for (let j = 0; j < $scope.veggieExtras.length; j++) {
                                if ($('#veggieExtras-' + i + ' option:selected').text() === $scope.veggieExtras[j].string) {
                                    veggieExtras.push({
                                        id: $scope.veggieExtras[j].mod.id,
                                        name: $scope.veggieExtras[j].mod.name,
                                        translated_name: $scope.veggieExtras[j].mod.translated_name,
                                        price: $scope.veggieExtras[j].mod.price,
                                        group_id: $scope.veggieExtras[j].mod.group_id
                                    });
                                    break;
                                }
                            }
                            extras.push(veggieExtras[i]);
                        }
                        if ($scope.specIns != null && whitespaceCheck.replace('\s', '').length != 0) {
                            specialInstructions = $scope.specIns;
                        }
                        let extrasPrice = 0.0;
                        for (let i = 0; i < extras.length; i++) {
                            extrasPrice += extras[i].price;
                        }
                        if ($scope.hasModifiers) {
                            itemPrice = parseFloat(item.price + mod.price + extrasPrice + $scope.ricePrice + $scope.bobaPrice);
                        }
                        else {
                            itemPrice = parseFloat(item.price + extrasPrice + $scope.ricePrice + $scope.bobaPrice);
                        }
                        order.order.push({
                            quantity: quantity,
                            item: {
                                id: item.id,
                                translated_name: item.translated_name,
                                price: item.price,
                                group_id: item.group_id
                            },
                            spice: $scope.spiceLevel,
                            hasSpice: parameters.hasSpice,
                            modifier: mod,
                            hasModifiers: $scope.hasModifiers,
                            extras: extras,
                            hasExtras: parameters.hasExtras,
                            extrasPrice: extrasPrice,
                            hasRice: parameters.hasRice,
                            riceType: $scope.riceType,
                            ricePrice: $scope.ricePrice,
                            bobaPrice: $scope.bobaPrice,
                            hasBoba: parameters.hasBoba,
                            specIns: specialInstructions,
                            indivPrice: itemPrice
                        });
                        order.cost = order.cost + (itemPrice * quantity);
                        order.tax = order.cost * (0.1);
                        if (orderInfo.type === 'Delivery') {
                            if (order.cost > 20.0) {
                                order.deliFee = 0;
                            }
                            else {
                                order.deliFee = Math.ceil(distance / 1609.34);
                            }
                        }
                        else {
                            order.deliFee = 0;
                        }
                        order.totalCost = order.cost + order.tax + order.deliFee;

                        SessionService.saveOrder(order.order, order.cost, order.tax, order.totalCost, order.deliFee);
                        $rootScope.$broadcast('addToOrder');
                        //$rootScope.$broadcast('updateOrder');
                        $uibModalInstance.close();
                    };
                    $scope.close = function () {
                        $uibModalInstance.close();
                    }
                }
            });
        },
        alert: function (message) {
            $uibModal.open({
                templateUrl: '../views/modals/alert_modal.html',
                controller: function ($scope, $uibModalInstance) {
                    $scope.message = message;
                    $scope.ok = function () {
                        $uibModalInstance.close();
                    };
                }
            });
        },
        guestModal: function () {
            $uibModal.open({
                templateUrl: '../views/modals/guest_modal.html',
                controller: function ($scope, $uibModalInstance, SessionService, LocationService, modalControllers) {
                    $scope.submit = function (guest) {
                        guest.phone = $('#phone').val();
                        let addressString = guest.address + ' ' + guest.zip;
                        LocationService.getDistanceFromRestaurant(addressString).then(function (distance) {
                            SessionService.setDeliveryDistance(distance);
                            SessionService.setCustomerInfo('GUEST', guest.email, guest.name, guest.address + '|-apt#->' + guest.apt, guest.phone, guest.zip);
                            modalControllers.orderStart('Welcome ' + guest.name + '!');
                            $uibModalInstance.close();
                        });
                    };
                    $scope.goToLogin = function () {
                        modalControllers.loginModal();
                        $uibModalInstance.close();
                    };
                    $scope.close = function () {
                        $uibModalInstance.close()
                    };
                }
            });
        },
        loginModal: function () {
            $uibModal.open({
                templateUrl: '../views/modals/login_modal.html',
                controller: function ($scope, $uibModalInstance, modalControllers, retrieve, LocationService, SessionService) {
                    $scope.submit = function (login) {
                        retrieve.login(login)
                            .then(function (data) {
                                if (data.success) {
                                    let info = {
                                        id: data.id,
                                        email: data.email,
                                        name: data.name,
                                        address: data.address,
                                        apt: data.apt,
                                        zip: data.zip,
                                        phone: data.phone
                                    };
                                    LocationService.getDistanceFromRestaurant(data.address + ' ' + data.zip).then(function (distance) {
                                        SessionService.setDeliveryDistance(distance);
                                        SessionService.setCustomerInfo(info.id, info.email, info.name, info.address + '|-apt#->' + info.apt, info.phone, info.zip);
                                        modalControllers.orderStart('Welcome back ' + info.name + '!');
                                        $uibModalInstance.close();
                                    });
                                }
                                else {
                                    modalControllers.alert(data.message);
                                }
                            });
                    };
                    $scope.goToSignUp = function () {
                        modalControllers.signupModal();
                        $uibModalInstance.close();
                    };
                    $scope.goToGuest = function () {
                        modalControllers.guestModal();
                        $uibModalInstance.close();
                    };
                    $scope.forgotPassword = function () {

                    };
                    $scope.close = function () {
                        $uibModalInstance.close()
                    };
                }
            });
        },
        orderInit: function () {
            $uibModal.open({
                templateUrl: '../views/modals/orderInit_modal.html',
                controller: function ($scope, $uibModalInstance, modalControllers) {
                    $scope.switchToLogin = function () {
                        modalControllers.loginModal();
                        $uibModalInstance.close();
                    };
                    $scope.continueAsGuest = function () {
                        modalControllers.guestModal();
                        $uibModalInstance.close();
                    };
                    $scope.close = function () {
                        $uibModalInstance.close();
                    };
                }
            });
        },
        orderPay: function () {
            $uibModal.open({
                templateUrl: '../views/modals/orderPay_modal.html',
                controller: function ($scope, $uibModalInstance, modalControllers, SessionService) {
                    $scope.order = angular.fromJson(SessionService.getOrder());
                    $scope.payment = null;
                    $scope.creditOnly = $scope.order.totalCost > 150.00;
                    if ($scope.creditOnly) {
                        $scope.payment = 'Credit';
                    }
                    $scope.card = [];
                    $scope.possibleMonth = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
                    $scope.possibleYear = [];
                    let currentYear = parseInt(moment().format('YY'));
                    for (let i = 0; i < 9; i++) {
                        $scope.possibleYear.push(currentYear + i + '');
                    }
                    $scope.submit = function () {
                        let info;
                        if ($scope.payment === 'Credit') {
                            info = {
                                number: $scope.card.number + '',
                                expMonth: $scope.card.expMonth,
                                expYear: $scope.card.expYear,
                                ccv: $scope.card.ccv
                            };
                            //info = {number:'4242424242424242', expMonth: '09', expYear: '20', ccv: '444'};
                        }
                        else {
                            info = 'Cash';
                        }
                        modalControllers.orderPreview(info);
                        $uibModalInstance.close();
                    };
                    $scope.testSubmit = function () {
                        let info;
                        if ($scope.payment === 'Credit') {
                            info = {number: '4242424242424242', expMonth: '09', expYear: '20', ccv: '444'};
                        }
                        else {
                            info = 'Cash';
                        }
                        modalControllers.orderPreview(info);
                        $uibModalInstance.close();
                    };
                    $scope.paymentTypeChanged = function (type) {
                        $scope.payment = type;
                    };
                    $scope.close = function () {
                        $uibModalInstance.close();
                    };
                }
            });
        },
        orderPreview: function (paymentInfo) {
            $uibModal.open({
                keyboard: false,
                backdrop: 'static',
                templateUrl: '../views/modals/orderPreview_modal.html',    // HTML for the modal
                controller: function ($rootScope, $scope, $location, $timeout, $uibModalInstance, modalControllers, SessionService, retrieve) {
                    $scope.order = angular.fromJson(SessionService.getOrder());

                    $scope.customerInfo = angular.fromJson(SessionService.getCustomerInfo());
                    $scope.address = $scope.customerInfo.address.split('|-apt#->')[0];
                    $scope.apt = $scope.customerInfo.address.split('|-apt#->')[1];
                    if ($scope.apt = 'undefined') {
                        $scope.apt = ''
                    }

                    $scope.orderInfo = angular.fromJson(SessionService.getOrderInfo());
                    $scope.isDelivery = ($scope.orderInfo.type !== 'Takeout');

                    $scope.paymentInfo = paymentInfo;
                    if (paymentInfo !== 'Cash') {
                        $scope.card = 'XXXX-XXXX-XXXX-' + paymentInfo.number.slice(-4);
                    }

                    $scope.confirm = function () {
                        let info = {
                            order: $scope.order,
                            customerInfo: $scope.customerInfo,
                            orderInfo: $scope.orderInfo,
                            isDelivery: $scope.isDelivery,
                            paymentInfo: $scope.paymentInfo
                        };
                        $rootScope.$broadcast('wait', true);
                        retrieve.payOrder(info)
                            .then(function (data) {
                                $rootScope.$broadcast('wait', false);
                                if (data.success) {
                                    modalControllers.alert(data.message);
                                    endOrder();
                                    $uibModalInstance.close();
                                }
                                else {
                                    modalControllers.alert(data.message);
                                }
                            })
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.close();
                    };
                    let endOrder = function () {
                        $scope.order = [];
                        $scope.cost = 0;
                        $scope.tax = 0;
                        $scope.totalCost = 0;
                        $scope.deliFee = 0;
                        SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
                        SessionService.clear();
                    };
                    $scope.editOrderInfo = function () {
                        modalControllers.orderSetUp();
                        $uibModalInstance.close();
                    };
                    $scope.editPaymentInfo = function () {
                        modalControllers.orderPay();
                        $uibModalInstance.close();
                    };
                    $scope.editCustomerInfo = function () {
                        modalControllers.userModal();
                        $uibModalInstance.close();
                    }
                }
            });
        },
        orderSetUp: function () {
            $uibModal.open({
                templateUrl: '../views/modals/orderSetUp_modal.html',
                controller: function ($rootScope, $scope, $location, $uibModalInstance, SessionService, modalControllers) {
                    let newOrder = true;
                    $scope.distance = SessionService.getDeliveryDistance();
                    $scope.deliAvail = $scope.distance <= 8046.72;
                    $scope.type = null;
                    $scope.selectedDate = null;
                    $scope.selectedTime = null;
                    $scope.dateLabel = 'Please Select a Specified Date';
                    $scope.timeLabel = 'Please Select a Specified Time';
                    if (!$scope.deliAvail) {
                        $scope.type = 'Takeout';
                    }
                    let dstCheck;
                    if (moment().isDST()) {
                        dstCheck = '-0400';
                    }
                    else {
                        dstCheck = '-0500';
                    }
                    let currentDate = new Date(moment().utcOffset(dstCheck)).getDay();
                    let currentHour = new Date(moment().utcOffset(dstCheck)).getHours();

                    switch (currentDate) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            if ($scope.type === 'Takeout') {
                                $scope.tooLate = currentHour > 20;
                            }
                            else {
                                $scope.tooLate = currentHour > 19;
                            }
                            break;
                        case 5:
                        case 6:
                            if ($scope.type === 'Takeout') {
                                $scope.tooLate = currentHour > 21;
                            }
                            else {
                                $scope.tooLate = currentHour > 20;
                            }
                            break;
                    }
                    createDates();

                    function createDates() {
                        if ($scope.tooLate) {
                            $scope.possibleDate = [
                                {
                                    string: moment().utcOffset(dstCheck).add(1, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(1, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(2, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(2, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(3, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(3, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(4, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(4, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(5, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(5, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(6, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(6, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(7, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(7, 'd')
                                }
                            ];
                        }
                        else {
                            $scope.possibleDate = [
                                {string: 'Today', date: moment().utcOffset(dstCheck)},
                                {
                                    string: moment().utcOffset(dstCheck).add(1, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(1, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(2, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(2, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(3, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(3, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(4, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(4, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(5, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(5, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(6, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(6, 'd')
                                },
                                {
                                    string: moment().utcOffset(dstCheck).add(7, 'd').format("dddd, MMMM Do YYYY"),
                                    date: moment().utcOffset(dstCheck).add(7, 'd')
                                }
                            ];
                        }
                    };


                    $scope.dateSelected = function (date) {
                        $scope.selectedDate = null;
                        $scope.selectedDate = date;
                        if (date.string === 'Today') {
                            $scope.setTimeForToday($scope.selectedDate);
                        }
                        else {
                            $scope.setTimeForFuture($scope.selectedDate);
                        }
                        $scope.selectedTime = null;
                        $scope.timeLabel = 'Please Select a Specified Time';
                    };
                    $scope.setTimeForToday = function (date) {
                        $scope.possibleTime = [];
                        let time = moment().utcOffset(dstCheck);
                        let dayInt = new Date(date.date).getDay();
                        let buffer = 15 - (time.minute() % 15);
                        if ($scope.type === 'Delivery') {
                            time.add(buffer + 75, 'm');
                        }
                        else {
                            time.add(buffer + 30, 'm');
                        }

                        if (time.hour() === 10 && time.minute() >= 30) {
                            time.hour(11)
                        }
                        else if (time.hour() < 11) {
                            time.hour(11);
                            time.minute(0);
                        }

                        if (dayInt === 6 || dayInt === 5) {
                            $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            while (time.hour() < 22) {
                                time.add(15, 'm');
                                $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            }
                        }
                        else {
                            $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            while (time.hour() < 21) {
                                time.add(15, 'm');
                                $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            }
                            while (time.minute() < 30) {
                                time.add(15, 'm');
                                $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            }
                        }
                    };
                    $scope.setTimeForFuture = function (date) {
                        $scope.possibleTime = [];
                        let dayInt = new Date(date.date).getDay();
                        let time = moment().utcOffset(dstCheck);
                        if (dayInt === 0) {
                            time.hour(16).minute(0);
                            $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            while (time.hour() < 21) {
                                time.add(15, 'm');
                                $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            }
                            while (time.minute() < 30) {
                                time.add(15, 'm');
                                $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            }
                        }
                        else if (dayInt === 6 || dayInt === 5) {
                            time.hour(11).minute(0);
                            $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            while (time.hour() < 22) {
                                time.add(15, 'm');
                                $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            }
                        }
                        else {
                            time.hour(11).minute(0);
                            $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            while (time.hour() < 21) {
                                time.add(15, 'm');
                                $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            }
                            while (time.minute() < 30) {
                                time.add(15, 'm');
                                $scope.possibleTime.push({string: time.format('h:mm a'), hour: time.hour()});
                            }
                        }
                    };
                    $scope.timeSelected = function (time) {
                        $scope.selectedTime = null;
                        $scope.selectedTime = time;
                    };
                    $scope.orderTypeChanged = function (type) {
                        $scope.type = null;
                        $scope.type = type;
                        createDates();
                        $scope.selectedTime = null;
                        $scope.timeLabel = 'Please Select a Specified Time';
                        $scope.selectedDate = null;
                        $scope.dateLabel = 'Please Select a Specified Date';
                    };
                    $scope.goToUserPage = function () {
                        modalControllers.userModal();
                        $uibModalInstance.close()
                    };
                    $scope.continue = function () {
                        if (SessionService.isOrdering()) {
                            newOrder = false;
                        }
                        SessionService.setOrderInfo($scope.type, $scope.selectedDate, $scope.selectedTime);
                        SessionService.setOrdering(true);
                        $location.url('/Home/menu');
                        if (newOrder) {
                            $rootScope.$broadcast('createOrder');
                        }
                        else {
                            $rootScope.$broadcast('updateInfo');
                        }
                        $uibModalInstance.close();
                    };
                    $scope.close = function () {
                        $uibModalInstance.close()
                    };
                }
            });
        },
        orderStart: function (message) {
            $uibModal.open({
                templateUrl: '../views/modals/orderStart_modal.html',
                controller: function ($scope, $uibModalInstance, modalControllers, SessionService) {
                    $scope.message = message;
                    $scope.startOrder = function () {
                        modalControllers.orderSetUp();
                        $uibModalInstance.close();
                    };
                    $scope.close = function () {
                        $uibModalInstance.close();
                    };
                }
            });
        },
        shopCart: function () {
            $uibModal.open({
                templateUrl: '../views/modals/shopCart_modal.html',
                controller: function ($scope, $uibModalInstance, $location, modalControllers, SessionService, $rootScope) {
                    $scope.hasOrder = SessionService.hasOrder();
                    let orderInfo = angular.fromJson(SessionService.getOrderInfo());
                    let distance = SessionService.getDeliveryDistance();
                    if ($scope.hasOrder) {
                        let order = angular.fromJson(SessionService.getOrder());
                        $scope.order = order.order;
                        $scope.cost = order.cost;
                        $scope.tax = order.tax;
                        console.log(orderInfo.type === 'Delivery');
                        if (orderInfo.type === 'Delivery') {
                            if ($scope.cost > 20.0) {
                                $scope.deliFee = 0.0;
                            }
                            else {
                                $scope.deliFee = Math.ceil(distance / 1609.34);
                                console.log($scope.deliFee)
                            }
                        }
                        else {
                            $scope.deliFee = 0.0;
                        }
                        $scope.totalCost = $scope.cost + $scope.tax + $scope.deliFee;
                        SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
                    }

                    /*+1 of desired item to order*/
                    $scope.increaseQty = function (index) {
                        $scope.order[index].quantity = parseInt($scope.order[index].quantity) + 1;
                        $scope.cost = $scope.cost + parseFloat($scope.order[index].indivPrice);
                        if (orderInfo.type === 'Delivery') {
                            if ($scope.cost > 20.0) {
                                $scope.deliFee = 0;
                            }
                            else {
                                $scope.deliFee = Math.ceil(distance / 1609.34);
                                console.log($scope.deliFee)
                            }
                        }
                        else {
                            $scope.deliFee = 0;
                        }
                        $scope.tax = $scope.cost * (0.1);
                        $scope.totalCost = $scope.tax + $scope.cost + $scope.deliFee;
                        SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
                    };

                    /*-1 of desired item to order*/
                    $scope.decreaseQty = function (index) {
                        if (parseInt($scope.order[index].quantity) > 1) {
                            $scope.order[index].quantity = parseInt($scope.order[index].quantity) - 1;
                            $scope.cost = $scope.cost - parseFloat($scope.order[index].indivPrice);
                            if (orderInfo.type === 'Delivery') {
                                if ($scope.cost > 20.0) {
                                    $scope.deliFee = 0;
                                }
                                else {
                                    $scope.deliFee = Math.ceil(distance / 1609.34);
                                    console.log($scope.deliFee)
                                }
                            }
                            else {
                                $scope.deliFee = 0;
                            }
                            $scope.tax = $scope.cost * (0.1);
                            $scope.totalCost = $scope.tax + $scope.cost + $scope.deliFee;
                            SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
                        }
                    };

                    /*Removes entire quantity of item from order*/
                    $scope.remove = function (index) {
                        $scope.cost = $scope.cost - parseFloat($scope.order[index].indivPrice * $scope.order[index].quantity);
                        if (orderInfo.type === 'Delivery') {
                            if ($scope.cost > 20.0) {
                                $scope.deliFee = 0;
                            }
                            else {
                                $scope.deliFee = Math.ceil(distance / 1609.34);
                                console.log($scope.deliFee)
                            }
                        }
                        else {
                            $scope.deliFee = 0;
                        }
                        $scope.tax = $scope.cost * (0.1);
                        $scope.totalCost = $scope.tax + $scope.cost + $scope.deliFee;
                        $scope.order.splice(index, 1);
                        SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
                        if ($scope.cost < 0.01) {
                            SessionService.removeOrder();
                            $scope.hasOrder = false;
                        }
                    };

                    $scope.orderPay = function () {
                        modalControllers.orderPay();
                        $uibModalInstance.close()
                    };

                    $scope.clear = function () {
                        $scope.order = [];
                        $scope.cost = 0;
                        $scope.tax = 0;
                        $scope.totalCost = 0;
                        $scope.deliFee = 0;
                        SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
                        SessionService.removeOrder();
                        $uibModalInstance.close()
                    };

                    $scope.close = function () {
                        $uibModalInstance.close();
                    };

                    $scope.menu = function () {
                        $location.path("/Home/menu");
                        $uibModalInstance.close();
                    };

                    $scope.cancel = function () {
                        $scope.order = [];
                        $scope.cost = 0;
                        $scope.tax = 0;
                        $scope.totalCost = 0;
                        $scope.deliFee = 0;
                        SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
                        SessionService.clear();
                        $rootScope.$broadcast('cancelOrder');
                        $uibModalInstance.close();
                    };
                }
            });
        },
        signupModal: function () {
            $uibModal.open({
                templateUrl: '../views/modals/signup_modal.html',
                controller: function ($scope, $uibModalInstance, modalControllers, retrieve, LocationService, SessionService) {
                    $scope.submit = function (newUser) {
                        if ($scope.form.$valid) {
                            newUser.phone = $('#phone').val();
                            let addressString = newUser.address + ' ' + newUser.zip;
                            LocationService.getDistanceFromRestaurant(addressString).then(function (distance) {
                                SessionService.setDeliveryDistance(distance);
                                retrieve.signup(newUser)
                                    .then(function (data) {
                                        if (data.success) {
                                            SessionService.setCustomerInfo(data.id, newUser.email, newUser.name, newUser.address + '|-apt#->' + newUser.apt, newUser.phone, newUser.zip);
                                            modalControllers.orderStart('Welcome ' + newUser.name + '!');
                                            $uibModalInstance.close();
                                        }
                                        else {
                                            modalControllers.alert(data.message);
                                        }
                                    });
                            });
                        }
                    };
                    $scope.goToLogin = function () {
                        modalControllers.loginModal();
                        $uibModalInstance.close();
                    };
                    $scope.close = function () {
                        $uibModalInstance.close()
                    };
                }
            });
        },
        userModal: function () {
            $uibModal.open({
                templateUrl: '../views/modals/user_modal.html',
                controller: function ($rootScope, $scope, $uibModalInstance, SessionService, LocationService, retrieve, modalControllers) {
                    $scope.editLabel = 'Edit';
                    $scope.editting = false;
                    $scope.customerInfo = angular.fromJson(SessionService.getCustomerInfo());
                    $scope.hasOrder = SessionService.isOrdering();
                    if ($scope.hasOrder) {
                        $scope.orderInfo = angular.fromJson(SessionService.getOrderInfo());
                        $scope.isDelivery = ($scope.orderInfo.type !== 'Takeout');
                    }
                    $scope.address = $scope.customerInfo.address.split('|-apt#->')[0];
                    $scope.apt = $scope.customerInfo.address.split('|-apt#->')[1];
                    if ($scope.apt === 'undefined') {
                        $scope.apt = 'N/A'
                    }
                    $scope.aptInit = function () {
                        if ($scope.customerInfo.address.split('|-apt#->')[1] === 'undefined') {
                            return '';
                        }
                        else {
                            return $scope.customerInfo.address.split('|-apt#->')[1];
                        }
                    };
                    $scope.pwChange = function () {
                        return $scope.editting && $scope.customerInfo.id !== 'GUEST';
                    };

                    $scope.edit = function () {
                        $scope.editting = true;
                    };
                    $scope.editOrderInfo = function () {
                        modalControllers.orderSetUp();
                        $uibModalInstance.close();
                    };
                    $scope.save = function (user) {
                        user.phone = $('#phone').val();
                        let addressString = user.address + ' ' + user.zip;
                        LocationService.getDistanceFromRestaurant(addressString).then(function (distance) {
                            SessionService.setDeliveryDistance(distance);
                            if ($scope.customerInfo.id === 'GUEST') {
                                SessionService.setCustomerInfo('GUEST', user.email, user.name, user.address + '|-apt#->' + user.apt, user.phone, user.zip);
                                modalControllers.alert('Your guest information has been updated!');
                                $scope.editting = false;
                                $scope.customerInfo = angular.fromJson(SessionService.getCustomerInfo());
                                $scope.address = $scope.customerInfo.address.split('|-apt#->')[0];
                                $scope.apt = $scope.customerInfo.address.split('|-apt#->')[1];
                                if ($scope.apt === 'undefined') {
                                    $scope.apt = 'N/A'
                                }
                            }
                            else {
                                let info = {
                                    id: parseInt($scope.customerInfo.id),
                                    user: user
                                };
                                retrieve.updateCustomer(info)
                                    .then(function (data) {
                                        if (data.success) {
                                            SessionService.setCustomerInfo(info.id, user.email, user.name, user.address + '|-apt#->' + user.apt, user.phone, user.zip);
                                            modalControllers.alert('Your account information has been updated!');
                                            $scope.editting = false;
                                            $scope.customerInfo = angular.fromJson(SessionService.getCustomerInfo());
                                            $scope.address = $scope.customerInfo.address.split('|-apt#->')[0];
                                            $scope.apt = $scope.customerInfo.address.split('|-apt#->')[1];
                                            if ($scope.apt === 'undefined') {
                                                $scope.apt = 'N/A'
                                            }
                                        }
                                        else {
                                            modalControllers.alert(data.message);
                                        }
                                    });

                            }
                        });
                    };
                    $scope.savePW = function (pw) {
                        let info = {
                            id: parseInt($scope.customerInfo.id),
                            pws: pw
                        };
                        retrieve.updatePassword(info)
                            .then(function (data) {
                                if (data.success) {
                                    $scope.editting = false;
                                    modalControllers.alert('Your account password has been updated!');
                                }
                                else {
                                    modalControllers.alert(data.message);
                                }
                            })
                    };
                    $scope.cancel = function () {
                        $scope.editting = false;
                    };
                    $scope.close = function () {
                        $uibModalInstance.close();
                    };
                    $scope.logout = function () {
                        if (SessionService.isOrdering()) {
                            $scope.order = [];
                            $scope.cost = 0.0;
                            $scope.tax = 0.0;
                            $scope.totalCost = 0.0;
                            $scope.deliFee = 0;
                            SessionService.saveOrder($scope.order, $scope.cost, $scope.tax, $scope.totalCost, $scope.deliFee);
                            SessionService.clear();
                            SessionService.logout();
                            $rootScope.$broadcast('cancelOrder');
                            $uibModalInstance.close();
                        }
                        else {
                            SessionService.logout();
                            $uibModalInstance.close();
                        }
                    }
                }
            });
        }
    }
});

app.directive('pwCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            let firstPassword = '#' + attrs.pwCheck;
            elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    let v = elem.val() === $(firstPassword).val();
                    ctrl.$setValidity('pwmatch', v);
                });
            });
        }
    }
}]);

app.directive('phoneInput', function ($filter, $browser) {
    return {
        require: 'ngModel',
        link: function ($scope, $element, $attrs, ngModelCtrl) {
            var listener = function () {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function (viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0, 10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function () {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function (event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function () {
                $browser.defer(listener);
            });
        }

    };
});

app.filter('tel', function () {
    return function (tel) {
        if (!tel) {
            return '';
        }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if (number) {
            if (number.length > 3) {
                number = number.slice(0, 3) + '-' + number.slice(3, 7);
            }
            else {
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else {
            return "(" + city;
        }

    };
});