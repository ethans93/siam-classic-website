let express = require('express');
let router = express.Router();
let fs = require('fs');
let _ = require('lodash');
let appFactory = require('../src/app-factory');
let bcrypt = require('bcryptjs');
let util = require('util');
let {Query} = require('pg');
let ApiContracts = require('authorizenet').APIContracts;
let ApiControllers = require('authorizenet').APIControllers;
let SDKConstants = require('authorizenet').Constants;
let premailer = require('premailer-api');
let base64 = require('base64-arraybuffer');

// let connectionString = '';
// const client = new pg.Client(connectionString);

module.exports.createSiamClassicApp = function () {
    return appFactory.createApp()
        .then(mountUrlEndpoints);
};

function mountUrlEndpoints(app) {
    app.logger.info("Creating url endpoints");

    app.get('/', function (req, res) {
        res.sendFile('index.html');
    });

    app.get('/testEmail/:email', function (req, res) {
        // Request this route from your browser by doing:
        //     localhost:2000/testEmail/<Your_Email_Here>
        //     aka: localhost:2000/testEmail/myname@example.com

        let mailOptions = {
            from: '"Siam Classic" <no-reply@siamclassic.com>', // sender address
            to: req.params.email, // list of receivers
            subject: 'Siam Classic Test', // Subject line
            text: 'Testing Siam Classic Server Emailer', // plain text body
            html: '<b>Testing HTML Functionality</b>' // html body
        };

        app.emailer.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });

        res.send("Email sent to: " + req.params.email);
    });

    app.post('/signup', function (req, res) {

        let salt = 10;
        let hashPass = bcrypt.hashSync(req.body.pw, salt);

        let address = req.body.address + '|-apt#->' + req.body.apt;

        let nameArray = req.body.name.split(' ');
        let firstName = nameArray[0];
        let lastName = '';

        for (let i = 1; i < nameArray.length; i++) {
            lastName = lastName + ' ' + nameArray[i];
        }

        app.database.query("SELECT check_duplicate_email($1);", [req.body.email], function (errA, result) {

            if (errA) {
                app.logger.error(new Date() + " | Error connecting to database, check_duplicate_email (function) for " + req.body.email + ": " + errA);
                res.status(500).json({success: false, message: 'Error on our end.\nPlease try again.'});
            }
            else {
                if (result.rows[0].check_duplicate_email) {
                    res.json({success: false, message: 'Email already in use'});
                }
                else {
                    app.database.query("SELECT create_customer($1, $2, $3, $4, $5, $6, $7, $8, $9);", [req.body.email, hashPass, req.body.name, firstName, lastName, req.body.phone, 'N/A', address, req.body.zip], function (errB, resultB) {
                        if (errB) {
                            app.logger.error(new Date() + " | Error connecting to database, create_customer (function)" + errB);
                            res.json({success: false, message: 'Error on our end.\nPlease try again.'})
                        }
                        else {
                            res.json({
                                success: true,
                                message: 'Account successfully created',
                                id: resultB.rows[0].create_customer
                            })
                        }
                    })
                }
            }

        });

        /*
        pg.connect(connectionString, (err, client, done) => {
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({success: false, data: err});
            }
            let queryA = client.query('SELECT check_duplicate_email($1);', [newUser.email]);
            queryA.on('row', (row) => {
                duplicate.push(row);
            });
            queryA.on('end', () => {
                duplicate = duplicate[0].check_duplicate_email;
                if(duplicate){
                    done();
                    res.send({result: false, message: 'Email already in use'});
                }
                else{
                    if(rawPass !== conPass){
                        done();
                        res.send({result: false, message: 'Passwords do not match'});
                    }
                    else{
                        let queryB = client.query('SELECT create_customer($1, $2, $3, $4, $5, $6, $7, $8)', [newUser.email, newUser.password, newUser.firstName, newUser.lastName, newUser.phone, newUser.dob, newUser.address, newUser.zip]);
                        queryB.on('end', () => {
                            done();
                            res.send({result: true, message: 'Account Successfully created!'});
                        })
                    }
                }
            });
        });
        */
    });

    app.post('/signin', function (req, res) {
        let user = [];
        app.database.query('SELECT * FROM signin_customer($1);', [req.body.email], function (errA, resultA) {
            if (errA) {
                app.logger.error(new Date() + " | Error connecting to database, signin_customer: " + errA);
                res.json({success: false, message: 'Error on our end.\nPlease try again.'})
            }
            else {
                resultA.rows.forEach(function (users) {
                    user.push(users);
                });
                if (user[0].result) {
                    bcrypt.compare(req.body.pw, user[0].password, function (errB, resultB) {
                        if (errB) {
                            app.logger.error(new Date() + " | Error with bcrypt: " + errB);
                            res.json({success: false, message: 'Error on our end.\nPlease try again.'})
                        }
                        else {
                            if (resultB === true) {
                                let address = user[0].address.split('|-apt#->')[0];
                                let apt = user[0].address.split('|-apt#->')[1];
                                res.send({
                                    success: true,
                                    id: user[0].id,
                                    email: user[0].email,
                                    name: user[0].name,
                                    address: address,
                                    apt: apt,
                                    zip: user[0].zip,
                                    phone: user[0].phone
                                });
                            }
                            else {
                                res.send({success: false, message: 'Incorrect Password'});
                            }
                        }
                    })
                }
                else {
                    res.send({success: false, message: 'Account does not exist'});
                }
            }
        });
        /*pg.connect(connectionString, (err, client, done) => {
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({success: false, data: err});
            }
            let query = client.query('SELECT * FROM signin_customer($1);', [data.email]);
            query.on('row', (row) => {
               user.push(row);
            });
            query.on('end', () => {
                user = user[0];
                if(user.result){
                    bcrypt.compare(data.password, user.password, function (err, result) {
                        if(result === true){
                            let address = user.address.split('|-apt#->');
                            let apt = user.address.split('|-apt#->')[1];
                            done();
                            res.send({result: true, email: user.email, name: user.name, address: address, apt: apt, zip: user.zip, phone: user.phone});
                        }
                        else{
                            done();
                            res.send({result: false, message: 'Incorrect Password'});
                        }
                    });
                }
                else{
                    done();
                    res.send({result: false, message: 'Account does not exist'});
                }
            });
        });*/
    });

    app.post('/updateCustomer', function (req, res) {
        let address = req.body.user.address + '|-apt#->' + req.body.user.apt;

        let nameArray = req.body.user.name.split(' ');
        let firstName = nameArray[0];
        let lastName = '';

        for (let i = 1; i < nameArray.length; i++) {
            lastName = lastName + ' ' + nameArray[i];
        }

        app.database.query("SELECT check_duplicate_email($1, $2);", [req.body.id, req.body.user.email], function (errA, result) {

            if (errA) {
                app.logger.error(new Date() + " | Error connecting to database, check_duplicate_email (function): " + errA);
                res.status(500).json({success: false, message: 'Could not connect to database, please try again later'});
            }
            else {
                if (result.rows[0].check_duplicate_email) {
                    res.json({success: false, message: 'That email is already in use'});
                }
                else {
                    app.database.query("SELECT update_customer($1, $2, $3, $4, $5, $6, $7, $8);", [req.body.id, req.body.user.email, req.body.user.name, firstName, lastName, req.body.user.phone, address, req.body.user.zip], function (errB, resultB) {
                        if (errB) {
                            app.logger.error(new Date() + " | Error connecting to database, update_customer (function): " + errB);
                            res.json({success: false, message: 'Could not connect to database, please try again later'})
                        }
                        else {
                            res.send({
                                success: true,
                                message: 'Account successfully created',
                            })
                        }
                    })
                }
            }

        });
    });

    app.post('/updatePassword', function (req, res) {
        let salt = 10;
        let hashPass = bcrypt.hashSync(req.body.pws.new, salt);
        app.database.query("SELECT password FROM public.customer_online WHERE id=($1)",[req.body.id], function (errA, resultA) {
            if (errA) {
                app.logger.error(new Date() + " | Error connecting to database, customer_online for id" + req.body.id + ": " + errA);
                res.status(500).json({success: false, message: "Could not connect to database, please try again later"});
            }
            else {
                let oldDb = resultA.rows[0].password;
                bcrypt.compare(req.body.pws.old, oldDb, function (errB, resultB) {
                    if (errB) {
                        app.logger.error(new Date() + " | Error changing passwords with bcrypt: " + errB);
                        res.json({success: false, message: 'Error on our end.\nPlease try again.'})
                    }
                    else {
                        if (resultB === true) {
                            app.database.query("SELECT update_customer_password($1, $2)",[req.body.id, hashPass], function (errC, resultC) {
                                if(errC){
                                    app.logger.error(new Date() + " | Error connecting to database, update_customer_passowrd (function): " + errC);
                                    res.status(500).json({success: false, message: "Could not connect to database, please try again later"});
                                }
                                else{
                                    res.json({success: true, message: 'Your password has been updated!'})
                                }
                            });
                        }
                        else {
                            res.send({success: false, message: 'The old password entered was incorrect'});
                        }
                    }
                })
            }
        });
    });

    app.get('/getMenuItems', function (req, res) {
        const menu = [];
        const menuSpecE = [], menuSpecA = [], menuApp = [], menuSS = [], menuNFR = [], menuEnt = [], menuHS = [],
            menuVeg = [], menuCur = [], menuDes = [], menuEsp = [], menuDri = [], menuLun = [];

        app.database.query('SELECT * FROM public.menu_item ORDER BY name ASC;', function (err, menu_item_result) {

            if (err) {
                app.logger.error(new Date() + "Error connecting to database, menu_item: " + err);
                res.status(500).json({success: false, message: "Error getting menu items, please refresh the page"});
            }
            else {
                // Collect all the menu items
                menu_item_result.rows.forEach(function (menuItem) {
                    if(menuItem.image !== null){
                        menuItem.image = base64.encode(menuItem.image);
                    }
                    menu.push(menuItem);
                });

                for (let i = 0; i < menu.length; i++) {
                    // Add the menu item the the correct food group
                    switch (menu[i].group_id) {
                        case 30:
                            menuApp.push(menu[i]);
                            break;
                        case 31:
                        case 32:
                            menuSS.push(menu[i]);
                            break;
                        case 19:
                        case 18:
                            menuNFR.push(menu[i]);
                            break;
                        case 25:
                            menuEnt.push(menu[i]);
                            break;
                        case 27:
                            menuHS.push(menu[i]);
                            break;
                        case 28:
                            menuVeg.push(menu[i]);
                            break;
                        case 29:
                            menuCur.push(menu[i]);
                            break;
                        case 20:
                        case 21:
                            menuDes.push(menu[i]);
                            break;
                        case 39:
                            menuEsp.push(menu[i]);
                            break;
                        case 47:
                            menuSpecE.push(menu[i]);
                            break;
                        case 88:
                            menuDri.push(menu[i]);
                            break;
                        case 86:
                            menuLun.push(menu[i]);
                            break;
                        case 94:
                            menuSpecA.push(menu[i]);
                            break;
                    }
                }
                let menuItems = {
                    menuSpecE: menuSpecE,
                    menuSpecA: menuSpecA,
                    menuApp: menuApp,
                    menuSS: menuSS,
                    menuNFR: menuNFR,
                    menuEnt: menuEnt,
                    menuHS: menuHS,
                    menuVeg: menuVeg,
                    menuCur: menuCur,
                    menuDes: menuDes,
                    menuEsp: menuEsp,
                    menuDri: menuDri,
                    menuLun: menuLun
                };

                res.send({success: true, menu_items: menuItems});
            }
        });

        /*
        let menuItemQuery = app.database.query(new Query('SELECT * FROM public.menu_item ORDER BY name ASC;'));

        menuItemQuery.on('row', (row) => {
            menu.push(row);
        });

        menuItemQuery.on('end', (row) => {
            let menuItemShiftQuery = app.database.query(new Query('SELECT * FROM public.menuitem_shift;'));

            menuItemShiftQuery.on('row', (row) => {
                menuItemShift.push(row);
            });

            menuItemShiftQuery.on('end', () => {

                // Determine whether it is currently lunch time
                let isLunchTime;
                if(data.isFuture){
                    isLunchTime = data.orderTime > 10 && data.orderTime < 15;
                }
                else{
                    isLunchTime = data.currentTime > 10 && data.currentTime < 15;
                }


                for (let i = 0; i < menu.length; i++) {
                    // If it is lunch time, we need to find each menu item in the menu
                    // item shifts and change its price to reflect lunch time pricing
                    if (isLunchTime) {
                        for (let k = 0; k < menuItemShift.length; k++) {
                            if (menuItemShift[k].menuitem_id == menu[i].id) {
                                if( menuItemShift[k].shift_id == 1){
                                    menu[i].price = menuItemShift[k].shift_price;
                                }
                            }
                        }
                    }

                    // Add the menu item the the correct food group
                    switch (menu[i].group_id) {
                        case 30:
                            menuApp.push(menu[i]);
                            break;
                        case 31:
                        case 32:
                            menuSS.push(menu[i]);
                            break;
                        case 19:
                        case 18:
                            menuNFR.push(menu[i]);
                            break;
                        case 25:
                            menuEnt.push(menu[i]);
                            break;
                        case 27:
                            menuHS.push(menu[i]);
                            break;
                        case 28:
                            menuVeg.push(menu[i]);
                            break;
                        case 29:
                            menuCur.push(menu[i]);
                            break;
                        case 20:
                            menuDes.push(menu[i]);
                            break;
                        case 21:
                            menuIce.push(menu[i]);
                            break;
                        case 22:
                            menuSod.push(menu[i]);
                            break;
                        case 23:
                        case 39:
                            menuCof.push(menu[i]);
                            break;
                        case 24:
                            menuJu.push(menu[i]);
                            break;
                        case 45:
                        case 46:
                            menuSaX.push(menu[i]);
                            break;
                        case 47:
                            menuSpec.push(menu[i]);
                    }
                }
                let menuItems = {
                    menuSpec: menuSpec,
                    menuApp: menuApp,
                    menuSS: menuSS,
                    menuNFR: menuNFR,
                    menuEnt: menuEnt,
                    menuHS: menuHS,
                    menuVeg: menuVeg,
                    menuCur: menuCur,
                    menuDes: menuDes,
                    menuIce: menuIce,
                    menuCof: menuCof,
                    menuJu: menuJu,
                    menuSod: menuSod,
                    menuSaX: menuSaX
                };

                res.send({menu_items: menuItems});
            });
        });
        */
    });

    app.get('/getMenuItemsSpecials', function (req, res) {
        let menu = [];

        app.database.query('SELECT * FROM public.menu_item WHERE group_id=47 OR group_id=94 ORDER BY translated_name ASC;', function (err, menu_items_specials) {
            if(err){
                app.logger.error(new Date() + " | Error connecting to database, menu_item (getMenuItemsSpecials): " + err);
                res.status(500).json({success: false, message: "Error getting menu items, please refresh the page"});
            }
            else{
                // Collect all the menu items
                menu_items_specials.rows.forEach(function (menuItem) {
                    if(menuItem.image !== null){
                        menuItem.image = base64.encode(menuItem.image);
                    }
                    menu.push(menuItem);
                });

                res.send({success: true, menu_items: menu});
            }
        })
    });

    app.post('/getMenuItemsByGroups', function (req, res) {
        let menu = [];

        app.database.query('SELECT * FROM public.menu_item WHERE group_id=$1 ORDER BY name ASC;', [req.body.id], function (err, menu_item_result) {
            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, menu_item (getMenuItemsByGroups):" + err);
                res.status(500).json({success: false, message: "Error getting menu items"});
            }
            else {
                // Collect all the menu items
                menu_item_result.rows.forEach(function (menuItem) {
                    if(menuItem.image !== null){
                        menuItem.image = base64.encode(menuItem.image);
                    }
                    menu.push(menuItem);
                });

                res.send({success: true, menu_items: menu});
            }
        });

        /*
        let menuItemQuery = app.database.query(new Query('SELECT * FROM public.menu_item ORDER BY name ASC;'));

        menuItemQuery.on('row', (row) => {
            menu.push(row);
        });

        menuItemQuery.on('end', (row) => {
            let menuItemShiftQuery = app.database.query(new Query('SELECT * FROM public.menuitem_shift;'));

            menuItemShiftQuery.on('row', (row) => {
                menuItemShift.push(row);
            });

            menuItemShiftQuery.on('end', () => {

                // Determine whether it is currently lunch time
                let isLunchTime;
                if(data.isFuture){
                    isLunchTime = data.orderTime > 10 && data.orderTime < 15;
                }
                else{
                    isLunchTime = data.currentTime > 10 && data.currentTime < 15;
                }


                for (let i = 0; i < menu.length; i++) {
                    // If it is lunch time, we need to find each menu item in the menu
                    // item shifts and change its price to reflect lunch time pricing
                    if (isLunchTime) {
                        for (let k = 0; k < menuItemShift.length; k++) {
                            if (menuItemShift[k].menuitem_id == menu[i].id) {
                                if( menuItemShift[k].shift_id == 1){
                                    menu[i].price = menuItemShift[k].shift_price;
                                }
                            }
                        }
                    }

                    // Add the menu item the the correct food group
                    switch (menu[i].group_id) {
                        case 30:
                            menuApp.push(menu[i]);
                            break;
                        case 31:
                        case 32:
                            menuSS.push(menu[i]);
                            break;
                        case 19:
                        case 18:
                            menuNFR.push(menu[i]);
                            break;
                        case 25:
                            menuEnt.push(menu[i]);
                            break;
                        case 27:
                            menuHS.push(menu[i]);
                            break;
                        case 28:
                            menuVeg.push(menu[i]);
                            break;
                        case 29:
                            menuCur.push(menu[i]);
                            break;
                        case 20:
                            menuDes.push(menu[i]);
                            break;
                        case 21:
                            menuIce.push(menu[i]);
                            break;
                        case 22:
                            menuSod.push(menu[i]);
                            break;
                        case 23:
                        case 39:
                            menuCof.push(menu[i]);
                            break;
                        case 24:
                            menuJu.push(menu[i]);
                            break;
                        case 45:
                        case 46:
                            menuSaX.push(menu[i]);
                            break;
                        case 47:
                            menuSpec.push(menu[i]);
                    }
                }
                let menuItems = {
                    menuSpec: menuSpec,
                    menuApp: menuApp,
                    menuSS: menuSS,
                    menuNFR: menuNFR,
                    menuEnt: menuEnt,
                    menuHS: menuHS,
                    menuVeg: menuVeg,
                    menuCur: menuCur,
                    menuDes: menuDes,
                    menuIce: menuIce,
                    menuCof: menuCof,
                    menuJu: menuJu,
                    menuSod: menuSod,
                    menuSaX: menuSaX
                };

                res.send({menu_items: menuItems});
            });
        });
        */
    });

    app.get('/getMenuItems_Mobile', function (req, res) {
        const menu = [];
        const menuSpecE = [], menuSpecA = [], menuApp = [], menuSS = [], menuNFR = [], menuEnt = [], menuHS = [],
            menuVeg = [], menuCur = [], menuDes = [], menuEsp = [], menuDri = [], menuLun = [];

        app.database.query('SELECT id, name, description, unit_name, translated_name, barcode, buy_price, stock_amount, price, discount_rate, visible, disable_when_stock_amount_is_zero, sort_order, btn_color, text_color, show_image_only, fractional_unit, pizza_type, group_id, tax_id, recepie, pg_id, default_sell_portion FROM public.menu_item ORDER BY name ASC;', function (err, menu_item_result) {

            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, menu_item (getMenuItems_Mobile):" + err);
                res.status(500).json({success: false, message: "Error getting menu items"});
            }
            else {
                // Collect all the menu items
                menu_item_result.rows.forEach(function (menuItem) {
                    menu.push(menuItem);
                });

                for (let i = 0; i < menu.length; i++) {
                    // Add the menu item the the correct food group
                    switch (menu[i].group_id) {
                        case 30:
                            menuApp.push(menu[i]);
                            break;
                        case 31:
                        case 32:
                            menuSS.push(menu[i]);
                            break;
                        case 19:
                        case 18:
                            menuNFR.push(menu[i]);
                            break;
                        case 25:
                            menuEnt.push(menu[i]);
                            break;
                        case 27:
                            menuHS.push(menu[i]);
                            break;
                        case 28:
                            menuVeg.push(menu[i]);
                            break;
                        case 29:
                            menuCur.push(menu[i]);
                            break;
                        case 20:
                        case 21:
                            menuDes.push(menu[i]);
                            break;
                        case 39:
                            menuEsp.push(menu[i]);
                            break;
                        case 47:
                            menuSpecE.push(menu[i]);
                            break;
                        case 88:
                            menuDri.push(menu[i]);
                            break;
                        case 86:
                            menuLun.push(menu[i]);
                            break;
                        case 94:
                            menuSpecA.push(menu[i]);
                            break;
                    }
                }
                let menuItems = {
                    menuSpecE: menuSpecE,
                    menuSpecA: menuSpecA,
                    menuApp: menuApp,
                    menuSS: menuSS,
                    menuNFR: menuNFR,
                    menuEnt: menuEnt,
                    menuHS: menuHS,
                    menuVeg: menuVeg,
                    menuCur: menuCur,
                    menuDes: menuDes,
                    menuEsp: menuEsp,
                    menuDri: menuDri,
                    menuLun: menuLun
                };

                res.send({success: true, menu_items: menuItems});
            }
        });

        /*
        let menuItemQuery = app.database.query(new Query('SELECT * FROM public.menu_item ORDER BY name ASC;'));

        menuItemQuery.on('row', (row) => {
            menu.push(row);
        });

        menuItemQuery.on('end', (row) => {
            let menuItemShiftQuery = app.database.query(new Query('SELECT * FROM public.menuitem_shift;'));

            menuItemShiftQuery.on('row', (row) => {
                menuItemShift.push(row);
            });

            menuItemShiftQuery.on('end', () => {

                // Determine whether it is currently lunch time
                let isLunchTime;
                if(data.isFuture){
                    isLunchTime = data.orderTime > 10 && data.orderTime < 15;
                }
                else{
                    isLunchTime = data.currentTime > 10 && data.currentTime < 15;
                }


                for (let i = 0; i < menu.length; i++) {
                    // If it is lunch time, we need to find each menu item in the menu
                    // item shifts and change its price to reflect lunch time pricing
                    if (isLunchTime) {
                        for (let k = 0; k < menuItemShift.length; k++) {
                            if (menuItemShift[k].menuitem_id == menu[i].id) {
                                if( menuItemShift[k].shift_id == 1){
                                    menu[i].price = menuItemShift[k].shift_price;
                                }
                            }
                        }
                    }

                    // Add the menu item the the correct food group
                    switch (menu[i].group_id) {
                        case 30:
                            menuApp.push(menu[i]);
                            break;
                        case 31:
                        case 32:
                            menuSS.push(menu[i]);
                            break;
                        case 19:
                        case 18:
                            menuNFR.push(menu[i]);
                            break;
                        case 25:
                            menuEnt.push(menu[i]);
                            break;
                        case 27:
                            menuHS.push(menu[i]);
                            break;
                        case 28:
                            menuVeg.push(menu[i]);
                            break;
                        case 29:
                            menuCur.push(menu[i]);
                            break;
                        case 20:
                            menuDes.push(menu[i]);
                            break;
                        case 21:
                            menuIce.push(menu[i]);
                            break;
                        case 22:
                            menuSod.push(menu[i]);
                            break;
                        case 23:
                        case 39:
                            menuCof.push(menu[i]);
                            break;
                        case 24:
                            menuJu.push(menu[i]);
                            break;
                        case 45:
                        case 46:
                            menuSaX.push(menu[i]);
                            break;
                        case 47:
                            menuSpec.push(menu[i]);
                    }
                }
                let menuItems = {
                    menuSpec: menuSpec,
                    menuApp: menuApp,
                    menuSS: menuSS,
                    menuNFR: menuNFR,
                    menuEnt: menuEnt,
                    menuHS: menuHS,
                    menuVeg: menuVeg,
                    menuCur: menuCur,
                    menuDes: menuDes,
                    menuIce: menuIce,
                    menuCof: menuCof,
                    menuJu: menuJu,
                    menuSod: menuSod,
                    menuSaX: menuSaX
                };

                res.send({menu_items: menuItems});
            });
        });
        */
    });

    app.post('/getMenuItemsImage', function (req, res) {
        let image = null;
        console.log('log');
        app.database.query('SELECT image FROM public.menu_item WHERE id=$1;', [req.body.id], function (err, menu_item_image) {
            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, menu_item (getMenuItemsImage): " + err);
                res.status(500).json({success: false, message: "Error getting menu items"});
            }
            else {
                console.log('log');
                menu_item_image.rows.forEach(function (menuItemImage) {
                    if(menuItemImage.image !== null){
                        image = base64.encode(menuItemImage.image);
                    }
                });
                res.send({success: true, image: image});
            }
        });
    });

    app.get('/getMenuGroups', function (req, res) {
        const menuGroup = [];

        app.database.query('SELECT * FROM public.menu_group;', function (err, result) {

            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, menu_group: " + err);
                res.status(500).json({success: false, message: "Error getting menu groups"});
            }
            else {
                result.rows.forEach(function (resultItem) {
                    menuGroup.push(resultItem);
                });

                res.send({success: true, menu_group: menuGroup});
            }

        });

    });

    app.get('/getMenuItemModifierGroup', function (req, res) {
        const menuItemModifierGroup = [];

        app.database.query('SELECT * FROM public.menuitem_modifiergroup;', function (err, result) {

            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, menuitem_modifiergroup: " + err);
                res.status(500).json({success: false, message: "Error getting menu modifier groups"});
            }
            else {
                result.rows.forEach(function (resultItem) {
                    menuItemModifierGroup.push(resultItem);
                });

                res.send({success: true, menuitem_modifiergroup: menuItemModifierGroup});
            }

        });

    });

    app.get('/getMenuModifiers', function (req, res) {
        const menuModifier = [];

        app.database.query('SELECT * FROM public.menu_modifier;', function (err, result) {

            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, menu_modifier: " + err);
                res.status(500).json({success: false, message: "Error getting menu modifiers"});
            }
            else {
                result.rows.forEach(function (resultItem) {
                    menuModifier.push(resultItem);
                });

                res.send({success: true, menu_modifier: menuModifier});
            }

        });

    });

    app.post('/authorizePayment', function (req, resolve) {
        let order = req.body.order;
        let orderItems = order.order;
        let customerInfo = req.body.customerInfo;
        let orderInfo = req.body.orderInfo;
        let isDelivery = req.body.isDelivery;
        let paymentInfo = req.body.paymentInfo;
        let menu = [];
        let menuModifiers = [];
        let orderID;

        Promise.all([getMenuItems, getMenuModifiers])
            .then((values)=>{
                menu = values[0];
                menuModifiers = values[1];
                let priceCheckPass = priceCheck(order, orderItems, isDelivery, menu, menuModifiers);

                if(priceCheckPass){
                    createNewOnlineOrder(customerInfo, orderItems, order, orderInfo)
                        .then((res)=>{
                            orderID = res;
                            if(paymentInfo === 'Cash'){
                                updateOnlineOrderPaid(orderID, paymentInfo);
                                sendEmail(order, orderItems, customerInfo, orderInfo, orderID)
                                    .then(function () {
                                        updateOnlineOrderEmail(orderID);
                                        resolve.json({success: true, message: "Order successfully went through! An order confirmation was sent to your provided email"});
                                    })
                                    .catch((error)=>{
                                        resolve.json({success: true, message: error});
                                    })
                            }
                            else{
                                creditAuthorize(paymentInfo, order)
                                    .then(function () {
                                        updateOnlineOrderPaid(orderID, 'Credit');
                                        sendEmail(order, orderItems, customerInfo, orderInfo, orderID)
                                            .then(function () {
                                                updateOnlineOrderEmail(orderID);
                                                resolve.json({success: true, message: "Order successful! A receipt was sent to the provided email"});
                                            })
                                            .catch((error)=>{
                                                resolve.json({success: true, message: error});
                                            })
                                    })
                                    .catch((error)=>{
                                        resolve.json({success: false, message: "Failed Transaction. " + error});
                                    })
                            }
                        })
                        .catch(function(){
                            resolve.json({success: false, message: "Error connecting to database, please try again"});
                        })
                }
                else{
                    resolve.json({success: false, message: "Sorry, there was an error on our side.\nPlease try again later"});
                }
            })
            .catch(function () {
                resolve.json({success: false, message: "Error connecting to database, please try again"});
            });

        /*app.database.query('SELECT * FROM public.menu_item ORDER BY id ASC;', function (err1, menu_item_result) {
            if (err1) {
                app.logger.error("Error getting data to validate customer order in /authorizePayment: " + err1);
                res.status(500).json({success: false, message: "Error connecting to database, please try again"});
            }
            else {
                menu_item_result.rows.forEach(function (menuItem) {
                    menu.push(menuItem);
                });
                app.database.query('SELECT * FROM public.menu_modifier ORDER BY id ASC;', function (err2, menu_modifier_result) {
                    if (err2) {
                        app.logger.error("Error getting data to validate customer order in /authorizePayment: " + err1);
                        res.status(500).json({success: false, message: "Error connecting to database, please try again"
                        });
                    }
                    else {
                        menu_modifier_result.rows.forEach(function (modifierItem) {
                            menuModifiers.push(modifierItem);
                        });

                        //let priceCheckPass = priceCheck(order, orderItems, isDelivery, menu, menuModifiers);
                        /!*if (priceCheckPass) {
                            app.database.query('SELECT create_order($1,$2,$3,$4,$5,$6,$7);', [customerInfo.email, customerInfo.name, JSON.stringify(orderItems), order.cost, order.tax, order.deliFee, order.totalCost], function (err, result) {
                                if (err) {
                                    app.logger.error("Error creating order");
                                    res.json({success: false, message: "Failed to connect to database.  Please try again."});
                                }
                                else {
                                    orderID = result.rows[0].create_order;
                                    if(paymentInfo === 'Cash'){

                                    }
                                    else{
                                        let merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
                                        merchantAuthenticationType.setName(process.env.AUTH_NAME);
                                        merchantAuthenticationType.setTransactionKey(process.env.AUTH_KEY);
                                        //Simon

                                        let creditCard = new ApiContracts.CreditCardType();
                                        creditCard.setCardNumber(paymentInfo.number);
                                        creditCard.setExpirationDate(paymentInfo.expMonth + paymentInfo.expYear);
                                        creditCard.setCardCode(paymentInfo.ccv);

                                        let paymentType = new ApiContracts.PaymentType();
                                        paymentType.setCreditCard(creditCard);

                                        let transactionRequestType = new ApiContracts.TransactionRequestType();
                                        transactionRequestType.setTransactionType(ApiContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
                                        transactionRequestType.setPayment(paymentType);
                                        transactionRequestType.setAmount(order.totalCost);

                                        let createRequest = new ApiContracts.CreateTransactionRequest();
                                        createRequest.setMerchantAuthentication(merchantAuthenticationType);
                                        createRequest.setTransactionRequest(transactionRequestType);

                                        let ctrl = new ApiControllers.CreateTransactionController(createRequest.getJSON());

                                        //Defaults to sandbox
                                        //ctrl.setEnvironment(SDKConstants.endpoint.production);

                                        ctrl.execute(function () {

                                            let apiResponse = ctrl.getResponse();

                                            let response = new ApiContracts.CreateTransactionResponse(apiResponse);

                                            let errMsg = 'Failed Transaction, Please try Again';

                                            if (response !== null) {
                                                if (response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
                                                    if (response.getTransactionResponse().getMessages() !== null) {
                                                        app.database.query("SELECT update_paid($1);", [orderID], function (err, result) {
                                                            if (err) {
                                                                app.logger.error("Error updating 'paid' for DB for order# " + orderID);
                                                                res.json({
                                                                    success: true,
                                                                    message: "There was an error with our email system but your order made it through! Please call the restaurant to confirm, 703-368-5647. Your order number was " + orderID
                                                                });
                                                            }
                                                            else {
                                                                // If all goes well send the email
                                                                // setup email data with unicode symbols
                                                                let emailString = createEmail(order, orderItems, customerInfo, orderInfo, orderID);
                                                                premailer.prepare({ html: emailString }, function (err, email) {
                                                                    let mailOptionsRes = {
                                                                        from: 'order@siamclassic.com',      // sender address
                                                                        to: 'ethanmcsteen@gmail.com',         // list of receivers
                                                                        subject: 'Order #' + orderID + ' - ' + (new Date()).toDateString(), // Subject line
                                                                        html: email.html         // plain text body
                                                                    };
                                                                    let mailOptionsCus = {
                                                                        from: 'siamclassic@siamclassic.com',      // sender address
                                                                        to: customerInfo.email,         // list of receivers
                                                                        subject: 'Siam Classic Online Order', // Subject line
                                                                        html: email.html
                                                                    };

                                                                    // send mail with defined transport object
                                                                    app.emailer.sendMail(mailOptionsRes, (error) => {
                                                                        if (error) {
                                                                            app.logger.error("Error sending restaurant email");
                                                                            res.json({
                                                                                success: true,
                                                                                message: "There was an error with our email system but your order made it through! Please call the restaurant to confirm, 703-368-5647. Your order number is " + orderID
                                                                            });
                                                                        }
                                                                        else {
                                                                            app.database.query("SELECT update_email_sent($1);", [orderID], function (err, result) {
                                                                                if (err) {
                                                                                    app.logger.error("Error updating 'email_sent' for DB for order# " + orderID);
                                                                                    res.json({
                                                                                        success: true,
                                                                                        message: "There was an error on our end but your order made it through! Please call the restaurant to confirm, 703-368-5647"
                                                                                    });
                                                                                }
                                                                                else {
                                                                                    app.emailer.sendMail(mailOptionsCus, (error) => {
                                                                                        if (error) {
                                                                                            app.logger.error("Error sending customer email");
                                                                                            res.json({
                                                                                                success: true,
                                                                                                message: "Your order went through but there was an error sending your receipt email, please confirm email address is valid.\nIf you have any questions please call 703-368-5647"
                                                                                            });
                                                                                        }
                                                                                        else {
                                                                                            app.logger.info("Customer Email sent");
                                                                                            res.json({
                                                                                                success: true,
                                                                                                message: "Order successfully went through! An order confirmation was sent to your provided email"
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                });

                                                            }
                                                        });
                                                    }
                                                    else {
                                                        app.logger.error('Failed Transaction2.');
                                                        if (response.getTransactionResponse().getErrors() !== null) {
                                                            app.logger.error('  -Error Code: ' + response.getTransactionResponse().getErrors().getError()[0].getErrorCode());
                                                            app.logger.error('  -Error message: ' + response.getTransactionResponse().getErrors().getError()[0].getErrorText());
                                                            errMsg = response.getTransactionResponse().getErrors().getError()[0].getErrorText()
                                                        }
                                                        res.json({success: false, message: 'Error: ' + errMsg});
                                                    }
                                                }
                                                else {
                                                    app.logger.error('Failed Transaction1. ');
                                                    if (response.getTransactionResponse() !== null && response.getTransactionResponse() !== null) {
                                                        app.logger.error('  -Error Code: ' + response.toString());
                                                        //app.logger.error('  -Error message: ' + response.getTransactionResponse().getError()[0].getErrorText());
                                                        errMsg = response.toString();
                                                    }
                                                    else {
                                                        app.logger.error('  -Error Code: ' + response.getMessages().getMessage()[0].getCode());
                                                        app.logger.error('  -Error message: ' + response.getMessages().getMessage()[0].getText());
                                                        errMsg = response.getMessages().getMessage()[0].getText()
                                                    }
                                                    res.json({success: false, message: 'Error: ' + errMsg, msg: response});
                                                }
                                            }
                                            else {
                                                app.logger.error('Could not connect to Authorize.net');
                                                res.json({
                                                    success: false,
                                                    message: "Error: Could not connect to credit card payment system"
                                                });
                                            }
                                        });
                                    }
                                }
                            });

                        }
                        else {
                            app.logger.error('Failed Transaction: Client and Server total prices did not match');
                            res.json({success: false, message: "Apologies, error on our side.  Please try again."});
                        }*!/
                    }
                });
            }
        });*/
    });
    let getMenuItems = new Promise((res, rej)=>{
        let menu = [];
        app.database.query('SELECT * FROM public.menu_item ORDER BY id ASC;', function (err, menu_item_result) {
            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, menu_item: " + err);
                rej("Error connecting to database, please try again");
            }
            else {
                menu_item_result.rows.forEach(function (menuItem) {
                    menu.push(menuItem);
                });
                res(menu);
            }
        });
    });
    let getMenuModifiers = new Promise((res, rej)=>{
        let menuModifiers = [];
        app.database.query('SELECT * FROM public.menu_modifier ORDER BY id ASC;', function (err, menu_modifier_result) {
            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, menu_modifier: " + err);
                rej("Error connecting to database, please try again");
            }
            else {
                menu_modifier_result.rows.forEach(function (modifierItem) {
                    menuModifiers.push(modifierItem);
                });
                res(menuModifiers);
            }
        });
    });
    let createNewOnlineOrder = function (customerInfo, orderItems, order, orderInfo) {
        return new Promise(function (res, rej) {
            app.database.query('SELECT create_order($1,$2,$3,$4,$5,$6,$7,$8,$9);', [customerInfo.email, customerInfo.name, JSON.stringify(customerInfo), JSON.stringify(orderInfo), JSON.stringify(orderItems), order.cost.toFixed(2), order.tax.toFixed(2), order.deliFee, order.totalCost], function (err, result) {
                if (err) {
                    app.logger.error(new Date() + " | Error connecting to database, create_order (function): " + err);
                    rej("Failed to connect to database.  Please try again.");
                }
                else {
                    res(result.rows[0].create_order);
                }
            });
        });
    };
    let priceCheck = function (_order, _orderItems, _isDelivery, _menu, _menuModifiers) {
        let order = _order;
        let orderItems = _orderItems;
        let isDelivery = _isDelivery;
        let menu = _menu;
        let menuModifiers = _menuModifiers;
        let dbPrice = 0.00;

        for (let i = 0; i < orderItems.length; i++) {
            let dbItemPrice = 0.00;
            let extras = [];
            if (orderItems[i].extras !== null) {
                extras = orderItems[i].extras;
            }
            for (let j = 0; j < menu.length; j++) {
                if (orderItems[i].item.id === menu[j].id) {
                    dbItemPrice += menu[j].price;
                    break;
                }
            }
            for (let j = 0; j < menuModifiers.length; j++) {
                if (orderItems[i].modifier.id === menuModifiers[j].id) {
                    dbItemPrice += menuModifiers[j].price;
                    break;
                }
            }
            for (let j = 0; j < extras.length; j++) {
                for (let k = 0; k < menuModifiers.length; k++) {
                    if (extras[j].id === menuModifiers[k].id) {
                        dbItemPrice += menuModifiers[k].price;
                    }
                }
            }
            dbItemPrice += orderItems[i].ricePrice;
            dbItemPrice += orderItems[i].bobaPrice;
            dbPrice += (dbItemPrice * orderItems[i].quantity);
        }
        dbPrice += (dbPrice * .10);
        if (isDelivery) {
            dbPrice += order.deliFee;
        }
        order.totalCost = order.totalCost.toFixed(2);
        dbPrice = dbPrice.toFixed(2);

        if(dbPrice === order.totalCost){
            return true;
        }
        else{
            app.logger.error(new Date() + ' | Price check fail -- Client$: ' + order.totalCost + ' - ' + 'Server$: ' + dbPrice);
            return false;
        }
    };
    let creditAuthorize = function (paymentInfo, order) {
        return new Promise(function (res, rej) {
            let merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
            merchantAuthenticationType.setName(process.env.AUTH_NAME);
            merchantAuthenticationType.setTransactionKey(process.env.AUTH_KEY);
            //Simon

            let creditCard = new ApiContracts.CreditCardType();
            creditCard.setCardNumber(paymentInfo.number);
            creditCard.setExpirationDate(paymentInfo.expMonth + paymentInfo.expYear);
            creditCard.setCardCode(paymentInfo.ccv);

            let paymentType = new ApiContracts.PaymentType();
            paymentType.setCreditCard(creditCard);

            let transactionRequestType = new ApiContracts.TransactionRequestType();
            transactionRequestType.setTransactionType(ApiContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
            transactionRequestType.setPayment(paymentType);
            transactionRequestType.setAmount(order.totalCost);

            let createRequest = new ApiContracts.CreateTransactionRequest();
            createRequest.setMerchantAuthentication(merchantAuthenticationType);
            createRequest.setTransactionRequest(transactionRequestType);

            let ctrl = new ApiControllers.CreateTransactionController(createRequest.getJSON());

            //Defaults to sandbox
            //ctrl.setEnvironment(SDKConstants.endpoint.production);

            ctrl.execute(function () {

                let apiResponse = ctrl.getResponse();

                let response = new ApiContracts.CreateTransactionResponse(apiResponse);

                let errMsg = 'Failed Transaction, Please try Again';

                if (response !== null) {
                    if (response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
                        if (response.getTransactionResponse().getMessages() !== null) {
                            res();
                        }
                        else {
                            app.logger.error(new Date() + 'Failed Transaction Alpha');
                            if (response.getTransactionResponse().getErrors() !== null) {
                                app.logger.error(new Date() + '  -Error Code: ' + response.getTransactionResponse().getErrors().getError()[0].getErrorCode());
                                app.logger.error(new Date() + '  -Error message: ' + response.getTransactionResponse().getErrors().getError()[0].getErrorText());
                                errMsg = response.getTransactionResponse().getErrors().getError()[0].getErrorText()
                            }
                            rej('Error: ' + errMsg);
                        }
                    }
                    else {
                        app.logger.error(new Date() + 'Failed Transaction Beta');
                        if (response.getTransactionResponse() !== null && response.getTransactionResponse() !== null) {
                            app.logger.error(new Date() + '  -Error Code: ' + response.toString());
                            //app.logger.error('  -Error message: ' + response.getTransactionResponse().getError()[0].getErrorText());
                            errMsg = response.toString();
                        }
                        else {
                            app.logger.error(new Date() + '  -Error Code: ' + response.getMessages().getMessage()[0].getCode());
                            app.logger.error(new Date() + '  -Error message: ' + response.getMessages().getMessage()[0].getText());
                            errMsg = response.getMessages().getMessage()[0].getText()
                        }
                        rej('Error: ' + errMsg);
                        //res.json({success: false, message: 'Error: ' + errMsg, msg: response});
                    }
                }
                else {
                    app.logger.error(new Date() + 'Could not connect to Authorize.net');
                    rej('Could not connect to Authorize.net')
                }
            });
        });
    };
    let updateOnlineOrderPaid = function (orderID, type) {
        app.database.query("SELECT update_paid($1, $2);", [orderID, type], function (err, result) {
            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, update_paid (function) for " + orderID + ": " + err);
            }
        });
    };
    let updateOnlineOrderEmail = function (orderID) {
        app.database.query("SELECT update_email_sent($1);", [orderID], function (err, result) {
            if (err) {
                app.logger.error(new Date() + " | Error connecting to database, update_email (function) for " + orderID + ": " + err);
            }
        });
    };
    let createEmail = function (_order, _orderItems, _customerInfo, _orderInfo, _orderID) {
        let order = _order;
        let orderItems = _orderItems;
        let customerInfo = _customerInfo;
        let orderInfo = _orderInfo;
        let orderID = _orderID;
        let emailStringFront = '<!DOCTYPE html>\n' +
            '<html>\n' +
            '<head>\n' +
            '<meta name="viewport" content="width=device-width" />\n' +
            '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n' +
            '<title>Billing e.g. invoices and receipts</title>\n' +
            '<style>\n' +
            '* {\n' +
            '            margin: 0;\n' +
            '            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;\n' +
            '            box-sizing: border-box;\n' +
            '            font-size: 14px;\n' +
            '        }\n' +
            '        img {\n' +
            '            max-width: 100%;\n' +
            '        }\n' +
            '        body {\n' +
            '            -webkit-font-smoothing: antialiased;\n' +
            '            -webkit-text-size-adjust: none;\n' +
            '            width: 100% !important;\n' +
            '            height: 100%;\n' +
            '            line-height: 1.6em;\n' +
            '            /* 1.6em * 14px = 22.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */\n' +
            '            /*line-height: 22px;*/\n' +
            '        }\n' +
            '        table td {\n' +
            '            vertical-align: top;\n' +
            '        }\n' +
            '        body {\n' +
            '            background-color: #f6f6f6;\n' +
            '        }\n' +
            '        .body-wrap {\n' +
            '            background-color: #f6f6f6;\n' +
            '            width: 100%;\n' +
            '        }\n' +
            '        .container {\n' +
            '            display: block !important;\n' +
            '            max-width: 600px !important;\n' +
            '            margin: 0 auto !important;\n' +
            '            /* makes it centered */\n' +
            '            clear: both !important;\n' +
            '        }\n' +
            '        .content {\n' +
            '            max-width: 600px;\n' +
            '            margin: 0 auto;\n' +
            '            display: block;\n' +
            '            padding: 20px;\n' +
            '        }\n' +
            '        .main {\n' +
            '            background-color: #fff;\n' +
            '            border: 1px solid #e9e9e9;\n' +
            '            border-radius: 3px;\n' +
            '        }\n' +
            '        .content-wrap {\n' +
            '            padding: 20px;\n' +
            '        }\n' +
            '        .content-block {\n' +
            '            padding: 0 0 20px;\n' +
            '        }\n' +
            '        .header {\n' +
            '            width: 100%;\n' +
            '            margin-bottom: 20px;\n' +
            '        }\n' +
            '        .footer {\n' +
            '            width: 100%;\n' +
            '            clear: both;\n' +
            '            color: #999;\n' +
            '            padding: 20px;\n' +
            '        }\n' +
            '        .footer p, .footer a, .footer td {\n' +
            '            color: #999;\n' +
            '            font-size: 12px;\n' +
            '        }\n' +
            '        h1, h2, h3 {\n' +
            '            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;\n' +
            '            color: #000;\n' +
            '            margin: 40px 0 0;\n' +
            '            line-height: 1.2em;\n' +
            '            font-weight: 400;\n' +
            '        }\n' +
            '        h1 {\n' +
            '            font-size: 32px;\n' +
            '            font-weight: 500;\n' +
            '            /* 1.2em * 32px = 38.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */\n' +
            '            /*line-height: 38px;*/\n' +
            '        }\n' +
            '        h2 {\n' +
            '            font-size: 24px;\n' +
            '            /* 1.2em * 24px = 28.8px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */\n' +
            '            /*line-height: 29px;*/\n' +
            '        }\n' +
            '        h3 {\n' +
            '            font-size: 18px;\n' +
            '            /* 1.2em * 18px = 21.6px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */\n' +
            '            /*line-height: 22px;*/\n' +
            '        }\n' +
            '        h4 {\n' +
            '            font-size: 14px;\n' +
            '            font-weight: 600;\n' +
            '        }\n' +
            '        p, ul, ol {\n' +
            '            margin-bottom: 10px;\n' +
            '            font-weight: normal;\n' +
            '        }\n' +
            '        p li, ul li, ol li {\n' +
            '            margin-left: 5px;\n' +
            '            list-style-position: inside;\n' +
            '        }\n' +
            '        a {\n' +
            '            color: #348eda;\n' +
            '            text-decoration: underline;\n' +
            '        }\n' +
            '        .btn-primary {\n' +
            '            text-decoration: none;\n' +
            '            color: #FFF;\n' +
            '            background-color: #348eda;\n' +
            '            border: solid #348eda;\n' +
            '            border-width: 10px 20px;\n' +
            '            line-height: 2em;\n' +
            '            /* 2em * 14px = 28px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */\n' +
            '            /*line-height: 28px;*/\n' +
            '            font-weight: bold;\n' +
            '            text-align: center;\n' +
            '            cursor: pointer;\n' +
            '            display: inline-block;\n' +
            '            border-radius: 5px;\n' +
            '            text-transform: capitalize;\n' +
            '        }\n' +
            '        .last {\n' +
            '            margin-bottom: 0;\n' +
            '        }\n' +
            '        .first {\n' +
            '            margin-top: 0;\n' +
            '        }\n' +
            '        .aligncenter {\n' +
            '            text-align: center;\n' +
            '        }\n' +
            '        .alignright {\n' +
            '            text-align: right;\n' +
            '        }\n' +
            '        .alignleft {\n' +
            '            text-align: left;\n' +
            '        }\n' +
            '        .clear {\n' +
            '            clear: both;\n' +
            '        }\n' +
            '        .alert {\n' +
            '            font-size: 16px;\n' +
            '            color: #fff;\n' +
            '            font-weight: 500;\n' +
            '            padding: 20px;\n' +
            '            text-align: center;\n' +
            '            border-radius: 3px 3px 0 0;\n' +
            '        }\n' +
            '        .alert a {\n' +
            '            color: #fff;\n' +
            '            text-decoration: none;\n' +
            '            font-weight: 500;\n' +
            '            font-size: 16px;\n' +
            '        }\n' +
            '        .alert.alert-warning {\n' +
            '            background-color: #FF9F00;\n' +
            '        }\n' +
            '        .alert.alert-bad {\n' +
            '            background-color: #D0021B;\n' +
            '        }\n' +
            '        .alert.alert-good {\n' +
            '            background-color: #68B90F;\n' +
            '        }\n' +
            '        .invoice {\n' +
            '            margin: 40px auto;\n' +
            '            text-align: left;\n' +
            '            width: 80%;\n' +
            '        }\n' +
            '        .invoice td {\n' +
            '            padding: 5px 0;\n' +
            '        }\n' +
            '        .invoice .invoice-items {\n' +
            '            width: 100%;\n' +
            '        }\n' +
            '        .invoice .invoice-items td {\n' +
            '            border-top: #eee 1px solid;\n' +
            '        }\n' +
            '        .invoice .invoice-items .total td {\n' +
            '            border-top: 2px solid #333;\n' +
            '            border-bottom: 2px solid #333;\n' +
            '            font-weight: 700;\n' +
            '        }\n' +
            '        @media only screen and (max-width: 640px) {\n' +
            '            body {\n' +
            '                padding: 0 !important;\n' +
            '            }\n' +
            '            h1, h2, h3, h4 {\n' +
            '                font-weight: 800 !important;\n' +
            '                margin: 20px 0 5px !important;\n' +
            '            }\n' +
            '            h1 {\n' +
            '                font-size: 22px !important;\n' +
            '            }\n' +
            '            h2 {\n' +
            '                font-size: 18px !important;\n' +
            '            }\n' +
            '            h3 {\n' +
            '                font-size: 16px !important;\n' +
            '            }\n' +
            '            .container {\n' +
            '                padding: 0 !important;\n' +
            '                width: 100% !important;\n' +
            '            }\n' +
            '            .content {\n' +
            '                padding: 0 !important;\n' +
            '            }\n' +
            '            .content-wrap {\n' +
            '                padding: 10px !important;\n' +
            '            }\n' +
            '            .invoice {\n' +
            '                width: 100% !important;\n' +
            '            }\n' +
            '        }\n' +
            '    </style>\n' +
            '</head>\n' +
            '\n' +
            '<body>\n' +
            '\n' +
            '<table class="body-wrap">\n' +
            '    <tr>\n' +
            '        <td></td>\n' +
            '        <td class="container" width="600">\n' +
            '            <div class="content">\n' +
            '                <table class="main" width="100%" cellpadding="0" cellspacing="0">\n' +
            '                    <tr>\n' +
            '                        <td class="content-wrap aligncenter">\n' +
            '                            <table width="100%" cellpadding="0" cellspacing="0">\n' +
            '                                <tr>\n' +
            '                                    <td class="content-block">\n' +
            '                                        <h1 class="aligncenter">Siam Classic</h1>\n' +
            '                                        <p>Best Choice For Thai Food</p>\n' +
            '                                    </td>\n' +
            '                                </tr>\n' +
            '                                <tr>\n' +
            '                                    <td class="content-block aligncenter">\n' +
            '                                        <table class="invoice">\n' +
            '                                            <tr>\n' +
            '                                                <td>Receipt #' + orderID + ' - ' + orderInfo.type + '<br>' + customerInfo.name + ' - ' + customerInfo.phone + '<br>For ' + orderInfo.date.string + ' - ' + orderInfo.time.string + '</td>\n' +
            '                                            </tr>\n' +
            '                                            <tr>\n' +
            '                                                <td>\n' +
            '                                                    <table class="invoice-items" cellpadding="0" cellspacing="0">';
        let emailStringBack = '                                     </tr>\n' +
            '                                                    </table>\n' +
            '                                                </td>\n' +
            '                                            </tr>\n' +
            '                                        </table>\n' +
            '                                    </td>\n' +
            '                                </tr>\n' +
            '                                <tr>\n' +
            '                                    <td class="content-block aligncenter">\n' +
            '                                        Siam Classic<br>9403 East Street, Manassas, VA<br>\n' +
            '                                        703-368-5647 <br>\n' +
            '                                        siamclassicva@gmail.com\n' +
            '                                    </td>\n' +
            '                                </tr>\n' +
            '                            </table>\n' +
            '                        </td>\n' +
            '                    </tr>\n' +
            '                </table>\n' +
            '                <div class="footer">\n' +
            '                    <table width="100%">\n' +
            '                        <tr>\n' +
            '                            <td class="aligncenter content-block">Questions? Call <b>703-368-5647</b></td>\n' +
            '                        </tr>\n' +
            '                    </table>\n' +
            '                </div></div>\n' +
            '        </td>\n' +
            '        <td></td>\n' +
            '    </tr>\n' +
            '</table>\n' +
            '\n' +
            '</body>\n' +
            '</html>';

        let emailStringBody = '';
        for(let i = 0; i < orderItems.length; i++){
            emailStringBody += '<tr>\n' +
                '                                                            <td><b style="color: #c50a0a">'+orderItems[i].item.translated_name+'</b> ($'+orderItems[i].item.price.toFixed(2)+')\n';
            if(orderItems[i].hasModifiers){
                emailStringBody += '<br>&emsp;Choice: '+ orderItems[i].modifier.translated_name;
                if(orderItems[i].modifier.price > 0){
                    emailStringBody += ' (+$'+orderItems[i].modifier.price.toFixed(2)+')';
                }
            }
            if(orderItems[i].hasSpice){
                emailStringBody += '<br>&emsp;Spicy Level: '+ orderItems[i].spice;
            }
            if(orderItems[i].hasExtras){
                for(let j = 0; j < orderItems[i].extras.length; j++){
                    if(j === 0){
                        emailStringBody += '<br>&emsp;Extras:<br>&emsp;&emsp;' + orderItems[i].extras[j].translated_name + ' (+$' + orderItems[i].extras[j].price.toFixed(2) + ')'
                    }
                    else{
                        emailStringBody += '<br>&emsp;&emsp;' + orderItems[i].extras[j].translated_name + ' (+$' + orderItems[i].extras[j].price.toFixed(2) + ')'
                    }
                }

            }
            if(orderItems[i].hasRice){
                emailStringBody += '<br>&emsp;Rice: '+ orderItems[i].riceType;
                if(orderItems[i].ricePrice > 0){
                    emailStringBody += ' (+$'+orderItems[i].ricePrice+')';
                }
            }
            if(orderItems[i].hasBoba){
                emailStringBody += '<br>&emsp;Added Boba (+$1.00)';
            }
            if(orderItems[i].specIns !== 'no special instructions'){
                emailStringBody += '<br>&emsp;Instructions: '+ orderItems[i].specIns;
            }
            emailStringBody +='                                                            <br><b>&emsp;Quantity:'+orderItems[i].quantity+'</b>\n'+
                '</td><td></td>\n' +
                '                                                            <td style="color: black" class="alignright"><b>$'+orderItems[i].indivPrice.toFixed(2)+'</b></td>\n' +
                '                                                        </tr>';
        }
        if(order.deliFee > 0){
            emailStringBody += '<tr class="total">\n' +
                '                                                                           <td></td>\n' +
                '                                                                           <td class="alignright">Sub-Total<br>Tax<br>Delivery Fee<br>Total</td>\n' +
                '                                                                           <td class="alignright">' + order.cost.toFixed(2) + '<br>' + order.tax.toFixed(2) + '<br>' + order.deliFee.toFixed(2) + '<br>' + order.totalCost + '</td>';
        }
        else{
            emailStringBody += '<tr class="total">\n' +
                '                                                            <td></td>\n' +
                '                                                            <td class="alignright">Sub-Total<br>Tax<br>Total</td>\n' +
                '                                                            <td class="alignright">' + order.cost.toFixed(2) + '<br>' + order.tax.toFixed(2) + '<br>' + order.totalCost + '</td>';
        }

        let emailString = emailStringFront + emailStringBody + emailStringBack;

        return emailString;
    };
    let sendEmail = function (order, orderItems, customerInfo, orderInfo, orderID) {
        return new Promise(function (res, rej) {
            let emailString = createEmail(order, orderItems, customerInfo, orderInfo, orderID);
            premailer.prepare({ html: emailString }, function (err, emailInline) {
                let email;
                if(err){
                    email = emailString
                }
                else{
                    email = emailInline.html
                }
                sendEmailToRes(orderID, email)
                    .then(function () {
                        sendEmailToCus(customerInfo, email)
                            .then(function () {
                                res()
                            })
                            .catch((error)=>{
                                rej(error)
                            });
                    })
                    .catch((error)=>{
                        rej(error)
                    });
            });
        });

    };
    let sendEmailToRes = function (orderID, email) {
        return new Promise(function (res, rej) {
            let mailOptionsRes = {
                from: process.env.RES_EMAILER,
                to: process.env.RES_EMAIL,
                cc: process.env.BU_EMAIL,
                subject: 'Order #' + orderID + ' - ' + (new Date()).toDateString(),
                html: email
            };
            app.emailer.sendMail(mailOptionsRes, (error) => {
                if (error) {
                    app.logger.error(new Date() + "Error sending restaurant email: " + error);
                    rej('Our email system ran into an error but your order made it through, please call 703-368-5647 to confirm order')
                }
                else {
                    app.logger.info("Restaurant Email sent");
                    res()
                }
            });
        });
    };
    let sendEmailToCus = function (customerInfo, email) {
        return new Promise(function (res, rej) {
            let mailOptionsCus = {
                from: process.env.CUS_EMAILER,      // sender address
                to: customerInfo.email,         // list of receivers
                subject: 'Siam Classic Online Order', // Subject line
                html: email
            };
            app.emailer.sendMail(mailOptionsCus, (error) => {
                if (error) {
                    app.logger.error(new Date() + "Error sending customer email: " + error);
                    rej('Our email system ran into an error but your order made it through.  Please call 703-368-5647 if you would like your receipt to be forwarded to you')
                }
                else {
                    app.logger.info("Customer Email sent");
                    res()
                }
            });
        })
    };

    return app;
}

